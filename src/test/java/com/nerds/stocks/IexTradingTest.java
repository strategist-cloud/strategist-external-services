package com.nerds.stocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.List;

import com.nerds.stocks.config.StrategistExternalServicesConfig;
import com.nerds.stocks.data.Financials;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.service.api.IEXtradingService;
import com.nerds.stocks.service.cache.ExchangeMap;
import com.nerds.stocks.service.cache.ExchangesClient;
import com.nerds.stocks.service.data.INTERVAL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import pl.zankowski.iextrading4j.api.exception.IEXTradingException;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = "com.nerds.stocks")
@ContextConfiguration(classes = { StrategistExternalServicesApplication.class })
@SpringBootTest(classes = { StrategistExternalServicesConfig.class, ExchangeMap.class })
public class IexTradingTest {

    @Autowired
    IEXtradingService iexTrading;

    @Autowired
    ExchangesClient exchangesCache;

    @Test
    public void testFundamentals() {
        Fundamentals ret = null;
        try {
            ret = iexTrading.getFundamentals("AAPL");
        } catch (IEXTradingException e) {
            assertEquals("Fundamentals not returned by IEX Free account: ", e.getMessage(),
                    "Message received from IEX Trading: The requested data is not available to free tier accounts. Please upgrade for access to this data.");
            return;
        }
        assertNotEquals(ret, null);
    }

    @Test
    public void testQuote() {
        Quote quote = iexTrading.getQuote("AAPL");
        assertNotEquals("Quote is returned ", quote, null);
        if (quote != null) {
            assertNotEquals("Quote contains current price: ", quote.getClosePrice(), 0.0d);
            assertNotEquals("Quote contains end time: ", quote.getEndTime(), null);
            assertNotEquals("Quote contains volume info: ", quote.getVolume(), 0l);
        }
    }

    @Test
    public void testHistoricalQuotes() {
        List<Quote> quotes = iexTrading.getHistoricalQuotes(null, "AAPL", INTERVAL._30MIN);
        assertNotEquals("Quote is returned ", quotes.size(), 0);
    }

    @Test
    public void testFinancials() {
        List<Financials> financials = null;
        try {
            financials = iexTrading.getFinancials("AAPL");
        } catch (IEXTradingException e) {
            assertEquals("Financials not returned by IEX Free account: ", e.getMessage(),
                    "Message received from IEX Trading: The requested data is not available to free tier accounts. Please upgrade for access to this data.");
            return;
        }
        assertNotEquals("Financials list returned: ", financials, null);

        if (financials != null) {
            assertNotEquals("Atleast one Financial info returned: ", financials.size(), 0);

            for (Financials financialInfo : financials) {
                assertNotEquals("Individual Object in financial list: ", financialInfo, null);

                if (financialInfo != null) {
                    assertNotEquals("Total Revenue was returned: ", financialInfo.getTotalRevenue(), 0.0d);
                }
            }
        }
    }

}