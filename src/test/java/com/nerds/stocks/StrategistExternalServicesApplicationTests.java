package com.nerds.stocks;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.service.cache.ExchangesClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class StrategistExternalServicesApplicationTests {

	@Autowired
	ExchangesClient exchangeClient;

	// @Test
	public void contextLoads() {
		logger.info("Started Exchange Tests");
		List<Exchange> exchanges = exchangeClient.getAllExchanges();
		assertEquals(2, exchanges.size());
	}

	static final Logger logger = LoggerFactory.getLogger(StrategistExternalServicesApplicationTests.class);
}
