package com.nerds.stocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Calendar;
import java.util.List;

import com.nerds.stocks.config.StrategistExternalServicesConfig;
import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Financials;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.data.Stats;
import com.nerds.stocks.service.api.YahooStockService;
import com.nerds.stocks.service.cache.ExchangeMap;
import com.nerds.stocks.service.data.INTERVAL;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;
import pl.zankowski.iextrading4j.api.exception.IEXTradingException;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = "com.nerds.stocks")
@ContextConfiguration(classes = { StrategistExternalServicesApplication.class })
@SpringBootTest(classes = { StrategistExternalServicesConfig.class, ExchangeMap.class })
@Slf4j
public class YahooServiceTest {

    @Autowired
    YahooStockService yahoo;

    @Autowired
    ExchangeMap exchangeCache;

    @Test
    public void testFundamentals() {
        Fundamentals ret = null;

        Exchange exchange = exchangeCache.getExchange("NYSE");
        ret = yahoo.getFundamentals(exchange, "AAPL", ret);

        assertNotEquals(ret, null);
    }

    @Test
    public void testQuote() {
        Exchange exchange = exchangeCache.getExchange("NYSE");
        Quote quote = yahoo.getCurrentPrice(exchange, "AAPL");
        assertNotEquals("Quote is returned ", quote, null);
        if (quote != null) {
            assertNotEquals("Quote contains current price: ", quote.getClosePrice(), 0.0d);
            assertNotEquals("Quote contains end time: ", quote.getEndTime(), null);
            assertNotEquals("Quote contains volume info: ", quote.getVolume(), 0l);
        }
    }

    @Test
    public void testFinancials() {
        Exchange exchange = exchangeCache.getExchange("NYSE");
        List<Financials> financials = null;
        try {
            financials = yahoo.getFinancials(exchange, "AAPL");
        } catch (IEXTradingException e) {
            assertEquals("Financials not returned by IEX Free account: ", e.getMessage(),
                    "Message received from IEX Trading: The requested data is not available to free tier accounts. Please upgrade for access to this data.");
            return;
        }
        assertNotEquals("Financials list returned: ", financials, null);

        if (financials != null) {
            assertNotEquals("Atleast one Financial info returned: ", financials.size(), 0);

            for (Financials financialInfo : financials) {
                assertNotEquals("Individual Object in financial list: ", financialInfo, null);

                if (financialInfo != null) {
                    assertNotEquals("Total Revenue was returned: ", financialInfo.getTotalRevenue(), 0.0d);
                }
            }
        }
    }

    @Test
    public void testDailyHistoricalQuotes() {
        Exchange exchange = exchangeCache.getExchange("NYSE");
        LocalDate now = LocalDate.now().minusYears(1);
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), 0, 0, 0);
        List<Quote> quotes = yahoo.getPricesFrom(exchange, "AAPL", calendar, INTERVAL.DAILY);

        assertNotEquals("QuoteList is not empty", null, quotes);
        if (quotes != null) {
            assertNotEquals("Quote List is not empty", 0, quotes.size());

            for (Quote quote : quotes) {
                assertNotEquals("Quote is not empty", null, quote);
                assertNotEquals(0.0d, quote.getClosePrice());
                assertNotEquals(null, quote.getEndTime());
                assertNotEquals(0l, quote.getVolume());
            }
        }

    }

    @Test
    public void testWeeklyHistoricalQuotes() {
        Exchange exchange = exchangeCache.getExchange("NYSE");
        LocalDate now = LocalDate.now().minusYears(1);
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), 0, 0, 0);
        List<Quote> quotes = yahoo.getPricesFrom(exchange, "AAPL", calendar, INTERVAL.DAILY);

        assertNotEquals("QuoteList is not empty", null, quotes);
        if (quotes != null) {
            assertNotEquals("Quote List is not empty", 0, quotes.size());

            for (Quote quote : quotes) {
                assertNotEquals("Quote is not empty", null, quote);
                assertNotEquals(0.0d, quote.getClosePrice());
                assertNotEquals(null, quote.getEndTime());
                assertNotEquals(0l, quote.getVolume());
            }
        }
    }

    @Test
    public void test5minHistoricalQuotes() {
        Exchange exchange = exchangeCache.getExchange("NYSE");
        LocalDate now = LocalDate.now().minusDays(5);
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), 0, 0, 0);
        List<Quote> quotes = yahoo.getPricesFrom(exchange, "AAPL", calendar, INTERVAL._5MIN);

        assertNotEquals("QuoteList is not empty", null, quotes);
        if (quotes != null) {
            assertNotEquals("Quote List is not empty", 0, quotes.size());

            for (Quote quote : quotes) {
                assertNotEquals("Quote is not empty", null, quote);
                assertNotEquals(0.0d, quote.getClosePrice());
                assertNotEquals(null, quote.getEndTime());
                log.info("Quotes Date: " + quote.getEndTime());
                // assertNotEquals(-1l, quote.getVolume());
            }
        }
    }

    @Test
    public void test30minHistoricalQuotes() {
        Exchange exchange = exchangeCache.getExchange("NYSE");
        LocalDate now = LocalDate.now().minusDays(30);
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), 0, 0, 0);
        List<Quote> quotes = yahoo.getPricesFrom(exchange, "AAPL", calendar, INTERVAL._30MIN);

        assertNotEquals("QuoteList is not empty", null, quotes);
        if (quotes != null) {
            assertNotEquals("Quote List is not empty", 0, quotes.size());

            for (Quote quote : quotes) {
                assertNotEquals("Quote is not empty", null, quote);
                assertNotEquals(0.0d, quote.getClosePrice());
                assertNotEquals(null, quote.getEndTime());
                // assertNotEquals(0l, quote.getVolume());
            }
        }
    }

    @Test
    public void testStats() {
        Exchange exchange = exchangeCache.getExchange("NYSE");
        Stats stats = yahoo.getStats(exchange, "AAPL");

        assertNotEquals(null, stats);
        if (stats != null) {
            assertNotEquals(null, stats.getBookValuePerShare());
            assertNotEquals(null, stats.getDividend());
            assertNotEquals(null, stats.getEBITDA());
            assertNotEquals(null, stats.getEarningsAnnouncement());
            assertNotEquals(null, stats.getEps());
            assertNotEquals(null, stats.getEpsEstimateCurrentYear());
            assertNotEquals(null, stats.getEpsEstimateNextQuarter());
            assertNotEquals(null, stats.getEpsEstimateNextYear());

            assertNotEquals(null, stats.getOneYearTargetPrice());
            assertNotEquals(null, stats.getPe());
            assertNotEquals(null, stats.getPeg());
            assertNotEquals(null, stats.getPriceBook());
            assertNotEquals(null, stats.getPriceSales());
            assertNotEquals(null, stats.getRevenue());
            assertNotEquals(null, stats.getSharesFloat());
            assertNotEquals(null, stats.getSharesOutstanding());
            assertNotEquals(null, stats.getSharesOwned());
            assertNotEquals(null, stats.getShortRatio());

        }

    }

    @Test
    public void searchTickersTest() {
        List<Fundamentals> fundamentals = yahoo.searchTickers("TVIX");

        for(Fundamentals fundamentals2: fundamentals) {
            log.info(fundamentals2.getExchange() + "/" + fundamentals2.getSymbol() + " -- " + fundamentals2.getName());

            fundamentals2 = yahoo.getFundamentals(exchangeCache.getExchange(fundamentals2.getExchange()), fundamentals2.getSymbol(), null);
            log.info("Name = " + fundamentals2.getName() + ", description = " + fundamentals2.getDescription() + ", year = " + fundamentals2.getYearFounded());

            Quote quote = yahoo.getCurrentPrice(exchangeCache.getExchange(fundamentals2.getExchange()), fundamentals2.getSymbol());

            log.info("Price of " + fundamentals2.getSymbol() + " is " + quote.getClosePrice());
        }
    }

}