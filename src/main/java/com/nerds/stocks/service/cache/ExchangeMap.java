package com.nerds.stocks.service.cache;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.nerds.stocks.data.Exchange;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
@ConfigurationProperties(prefix = "stocks.exchangemap")
public class ExchangeMap {
    Map<String, String> yahoo;
    Map<String, String> robinhood;
    Map<String, String> iex;

    @Autowired
    ExchangesClient exchangesClient;

    Map<String, Exchange> exchanges = Collections.emptyMap();

    private void loadExchanges() {
        try {
            List<Exchange> exchangeList = exchangesClient.getAllExchanges();
            this.exchanges = exchangeList.parallelStream()
                    .collect(Collectors.toMap(exchange -> exchange.getExchangeId(), exchange -> exchange));
            logger.info("Exchanges loaded are " + this.exchanges);
        } catch (Exception e) {
            logger.error("Error while querying the exchanges client ", e);
        }
    }

    public Exchange getExchange(String exchangeId) {
        if (CollectionUtils.sizeIsEmpty(exchanges)) {
            loadExchanges();
        }

        Exchange returned = exchanges.get(exchangeId);
        if (returned == null) {
            returned = new Exchange();
            returned.setExchangeId(exchangeId);
            returned.setExchangeName(exchangeId);
        }
        return returned;
    }

    public String getYahooExchange(String yahooExchangeId) {
        return getExchange(yahooExchangeId, yahoo);
    }

    public String getIexExchange(String iexExchangeId) {
        return getExchange(iexExchangeId, iex);
    }

    public String getRobinhoodExchange(String robinhoodExchangeId) {
        return getExchange(robinhoodExchangeId, robinhood);
    }

    private String getExchange(String givenExchangeId, Map<String, String> exchangeMap) {
        if (givenExchangeId == null) {
            givenExchangeId = "";
        }

        givenExchangeId = givenExchangeId.replaceAll("\\s", "");
        String exchangeId = exchangeMap.get(givenExchangeId);
        if (StringUtils.isEmpty(exchangeId)) {
            exchangeId = givenExchangeId;
        }
        return exchangeId;
    }

    public Map<String, String> getYahooExchangeMap() {
        return yahoo;
    }

    public void setYahooExchangeMap(Map<String, String> exchangeMap) {
        this.yahoo = exchangeMap;
    }

    public Map<String, String> getRobinhoodExchangeMap() {
        return robinhood;
    }

    public void setRobinhoodExchangeMap(Map<String, String> exchangeMap) {
        this.robinhood = exchangeMap;
    }

    static final Logger logger = LoggerFactory.getLogger(ExchangeMap.class);

}