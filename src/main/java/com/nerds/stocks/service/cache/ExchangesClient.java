package com.nerds.stocks.service.cache;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nerds.stocks.data.Exchange;

@FeignClient(name = "stocks-data-collector-service")
public interface ExchangesClient {

	@RequestMapping("/exchanges/getAll")
	public List<Exchange> getAllExchanges();

}
