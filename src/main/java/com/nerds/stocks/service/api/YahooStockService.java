package com.nerds.stocks.service.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Financials;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.data.Stats;
import com.nerds.stocks.data.yahoo.search.response.YahooSearchResponse;
import com.nerds.stocks.data.yahoo.v8.response.QuotesResponse;
import com.nerds.stocks.service.cache.ExchangeMap;
import com.nerds.stocks.service.data.INTERVAL;
import com.zavtech.morpheus.array.Array;
import com.zavtech.morpheus.frame.DataFrame;
import com.zavtech.morpheus.frame.DataFrameContent;
import com.zavtech.morpheus.frame.DataFrameContent.Vector;
import com.zavtech.morpheus.util.text.SmartFormat;
import com.zavtech.morpheus.util.text.printer.Printer;
import com.zavtech.morpheus.yahoo.YahooField;
import com.zavtech.morpheus.yahoo.YahooFieldExt;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.stock.StockDividend;
import yahoofinance.quotes.stock.StockQuote;
import yahoofinance.quotes.stock.StockStats;

@Component
public class YahooStockService {

    @Autowired
    ExchangeMap exchangesCache;

    static final Logger logger = LoggerFactory.getLogger(YahooStockService.class);

    private List<Quote> getQuotesUsingV8Api(Exchange exchange, String ticker, Calendar from, INTERVAL interval) {
        List<Quote> quotes = new ArrayList<Quote>();
        String url = "https://query1.finance.yahoo.com/v8/finance/chart/" + ticker
                + "?region=US&lang=en-US&includePrePost=false&corsDomain=finance.yahoo.com&.tsrc=finance";

        RestTemplate restTemplate = new RestTemplate();
        try {
            QuotesResponse response = restTemplate.getForObject(url, QuotesResponse.class);
            quotes = getQuotes(response);
        } catch (Exception e) {
            logger.error("Error while retrieving quotes from yahoo v8 rest API");
        }

        return quotes;
    }

    public List<Fundamentals> searchTickers(String searchstr) {
        List<Fundamentals> fundamentals = new ArrayList<Fundamentals>();
        String url = "https://query1.finance.yahoo.com/v1/finance/search?q=" + searchstr + "&quotesCount=6"
                        + "&newsCount=0&enableFuzzyQuery=false&quotesQueryId=tss_match_phrase_query&multiQuoteQueryId=multi_quote_single_token_query" 
                        + "&newsQueryId=news_cie_vespa&enableCb=true&enableNavLinks=true";
        logger.info("Searching yahoo for " + searchstr + " using " + url);

        RestTemplate restTemplate = new RestTemplate();
        try {
            YahooSearchResponse response = restTemplate.getForObject(url, YahooSearchResponse.class);
            if(response != null && !CollectionUtils.isEmpty(response.getQuotes())) {
                fundamentals = response.getQuotes().stream().map(quote -> getFundamentals(quote)).collect(Collectors.toList());
            }
        } catch (Exception e) {
            logger.error("Error while retrieving quotes from yahoo v8 rest API");
        }

        return fundamentals;
    }

    public Fundamentals getFundamentals(com.nerds.stocks.data.yahoo.search.response.Quote quote) {
        Fundamentals fundamentals = new Fundamentals();
        fundamentals.setName(quote.getShortname());
        fundamentals.setExchange(exchangesCache.getYahooExchange(quote.getExchange()));
        fundamentals.setSymbol(quote.getSymbol());
        return fundamentals;
    }

    public List<Quote> getPricesFrom(Exchange exchange, String ticker, Calendar from, INTERVAL interval) {
        List<Quote> quotes = new ArrayList<Quote>();
        if (interval == INTERVAL._5MIN || interval == INTERVAL._30MIN) {
            quotes = getQuotesUsingV8Api(exchange, ticker, from, interval);
        } else {
            Interval yahooInterval = getYahooInterval(interval);
            try {
                Stock stock = YahooFinance.get(ticker, from, yahooInterval);
                List<HistoricalQuote> historicalQuotes = stock.getHistory(from, yahooInterval);
                quotes = historicalQuotes.stream().map(q -> getQuote(q)).collect(Collectors.toList());
            } catch (Exception e) {
                logger.error("Error while trying to get stock Info from yahoo ", e);
            }
        }
        return quotes;
    }

    /**
     * @param ticker:       Stock symbol to looking for.
     * @param Fundamentals: Return object to load the data from yahoo.
     * @throws IOException
     */
    public Fundamentals getFundamentals(Exchange exchange, String ticker, Fundamentals ret) {
        if (ret == null)
            ret = new Fundamentals();
        Stock result = null;
        try {
            result = YahooFinance.get(ticker);
            ret.setName(result.getName());
            ret.setSymbol(ticker);
            ret.setExchange(exchangesCache.getYahooExchange(result.getStockExchange()));

            com.zavtech.morpheus.yahoo.YahooFinanceExt yahoo = new com.zavtech.morpheus.yahoo.YahooFinanceExt();
            DataFrame<String, YahooFieldExt> fundamentals = yahoo.getFundamentals(ticker);
            DataFrameContent<String, YahooFieldExt> companyInfo = fundamentals.data();

            ret.setName(companyInfo.getValue(0, YahooFieldExt.COMPANY_NAME));
            ret.setCity(companyInfo.getValue(0, YahooFieldExt.COMPANY_CITY));
            ret.setDescription(companyInfo.getValue(0, YahooFieldExt.COMPANY_DESC));
            ret.setIndustry(companyInfo.getValue(0, YahooFieldExt.COMPANY_INDUSTRY));
            ret.setSector(companyInfo.getValue(0, YahooFieldExt.COMPANY_SECTOR));
        } catch (Exception e) {
            logger.error("Error trying to retrieve fundalementals from yahoo", e);
        }

        return ret;
    }

    public Stats getStats(Exchange exchange, String ticker) {
        Stats stats = new Stats();
        Stock result = null;
        try {
            result = YahooFinance.get(ticker);
            if (result.getName() != null) {
                stats = getStats(result.getStats(true), result.getDividend(true));
            }
        } catch (Exception e) {
            logger.error("Error while retrieving stats object from yahoo", e);
        }
        return stats;
    }

    private BigDecimal getValue(Long longValue) {
        BigDecimal retValue = null;
        try {
            retValue = BigDecimal.valueOf(longValue);
        } catch (Exception e) {

        }
        return retValue;
    }

    private Stats getStats(StockStats statsObj, StockDividend dividend) {
        Stats stats = new Stats();

        stats.setSharesFloat(getValue(statsObj.getSharesFloat()));
        stats.setSharesOutstanding(getValue(statsObj.getSharesOutstanding()));
        stats.setSharesOwned(getValue(statsObj.getSharesOwned()));
        stats.setEps(statsObj.getEps());
        stats.setPe(statsObj.getPe());
        stats.setPeg(statsObj.getPeg());
        stats.setEpsEstimateCurrentYear(statsObj.getEpsEstimateCurrentYear());
        stats.setEpsEstimateNextQuarter(statsObj.getEpsEstimateNextQuarter());
        stats.setEpsEstimateNextYear(statsObj.getEpsEstimateNextYear());

        stats.setPriceBook(statsObj.getPriceBook());
        stats.setPriceSales(statsObj.getPriceSales());
        stats.setBookValuePerShare(statsObj.getBookValuePerShare());
        stats.setRevenue(statsObj.getRevenue());
        stats.setEBITDA(statsObj.getEBITDA());
        stats.setDividend(dividend.getAnnualYield());
        stats.setOneYearTargetPrice(statsObj.getOneYearTargetPrice());
        stats.setShortRatio(statsObj.getShortRatio());
        stats.setEarningsAnnouncement(statsObj.getEarningsAnnouncement());
        return stats;
    }

    public static Stats getStatsFromMorpheusYahoo(Exchange exchange, String ticker) {
        Stats stats = new Stats();
        try {

            com.zavtech.morpheus.yahoo.YahooFinanceExt yahoo = new com.zavtech.morpheus.yahoo.YahooFinanceExt();
            Array<String> tickers = Array.of(ticker);
            DataFrame<String, YahooField> statistics = yahoo.getStatistics(tickers);
            DataFrameContent<String, YahooField> statsObj = statistics.data();

            statistics.transpose().out().print(200, formats -> {
                final SmartFormat smartFormat = new SmartFormat();
                formats.setPrinter(Object.class, Printer.forObject(smartFormat::format));
            });

            // stats.setMarketCap(getValue(statsObj, 0, YahooField.ENTERPRISE_VALUE));
            stats.setSharesFloat(getValue(statsObj, 0, YahooField.SHARES_FLOAT));
            stats.setSharesOutstanding(getValue(statsObj, 0, YahooField.SHARES_OUTSTANDING));
            stats.setSharesOwned(getValue(statsObj, 0, YahooField.OWNER_PERCENT_INSIDER));
            stats.setEps(getValue(statsObj, 0, YahooField.EPS_DILUTED));
            stats.setPe(getValue(statsObj, 0, YahooField.PE_FORWARD));
            stats.setPeg(getValue(statsObj, 0, YahooField.PEG_RATIO));
            stats.setEpsEstimateCurrentYear(getValue(statsObj, 0, YahooField.EPS_ESTIMATE));
            stats.setEpsEstimateNextQuarter(getValue(statsObj, 0, YahooField.EPS_NEXT_QUARTER));
            stats.setEpsEstimateNextYear(getValue(statsObj, 0, YahooField.EPS_NEXT_YEAR));

            stats.setPriceBook(getValue(statsObj, 0, YahooField.PRICE_BOOK_RATIO));
            stats.setPriceSales(getValue(statsObj, 0, YahooField.PRICE_SALES_RATIO));
            stats.setBookValuePerShare(getValue(statsObj, 0, YahooField.BOOK_VALUE_PER_SHARE));
            stats.setRevenue(getValue(statsObj, 0, YahooField.ENTERPRISE_VALUE_REVENUE));
            stats.setEBITDA(getValue(statsObj, 0, YahooField.EBITDA));
            stats.setDividend(getValue(statsObj, 0, YahooField.DIVIDEND_PER_SHARE));
            // stats.setOneYearTargetPrice(getValue(statsObj, 0, YahooField.));
            stats.setShortRatio(getValue(statsObj, 0, YahooField.SHARES_SHORT_RATIO));
            // stats.setEarningsAnnouncement(statsObj.getDouble(0, YahooField.));
        } catch (Exception e) {
            logger.error("Error while getting stats from Yahoo", e);
        }
        return stats;
    }

    private static BigDecimal getValue(DataFrameContent<String, YahooField> statsObj, int rowIndex, YahooField field) {
        double retValue = 0.0d;
        try {
            retValue = statsObj.getDouble(rowIndex, field);
        } catch (Exception e) {
            logger.info("Error retrieving value for " + field.getName() + " from yahoo");
        }
        return BigDecimal.valueOf(retValue);
    }

    private static Double getValue(Vector<LocalDate, YahooFieldExt> statsObj, int rowIndex, YahooFieldExt field) {
        Double retValue = 0.0d;
        try {
            retValue = statsObj.getDouble(field);
            if (Double.isNaN(retValue)) {
                retValue = null;
            }
        } catch (Exception e) {
            logger.info("Error retrieving value for " + field.getName() + " from yahoo");
        }
        return retValue;
    }

    public Quote getCurrentPrice(Exchange exchange, String ticker) {
        Quote quote = new Quote();
        try {
            Stock yahooStockInfo = YahooFinance.get(ticker);
            quote = getQuote(yahooStockInfo.getQuote());
        } catch (Exception e) {
            logger.error("Error while trying to calculate current value for " + ticker, e);
        }
        return quote;
    }

    private Quote getQuote(StockQuote yahooQuoteInfo) {
        Quote quote = new Quote();
        quote.setClosePrice(yahooQuoteInfo.getPrice().doubleValue());
        quote.setOpenPrice(yahooQuoteInfo.getOpen().doubleValue());
        quote.setHighPrice(yahooQuoteInfo.getDayHigh().doubleValue());
        quote.setLowPrice(yahooQuoteInfo.getDayLow().doubleValue());
        quote.setStock(yahooQuoteInfo.getSymbol());
        quote.setVolume(yahooQuoteInfo.getVolume());
        quote.setEndTime(new Timestamp(yahooQuoteInfo.getLastTradeTime().getTimeInMillis()));

        return quote;
    }

    private Quote getQuote(HistoricalQuote q) {
        Quote quote = new Quote();
        Calendar date = q.getDate();
        Timestamp startTime = new Timestamp(date.getTimeInMillis());
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 0);
        Timestamp endTime = new Timestamp(date.getTimeInMillis());

        quote.setOpenPrice(q.getOpen().doubleValue());
        quote.setClosePrice(q.getAdjClose().doubleValue());
        quote.setHighPrice(q.getHigh().doubleValue());
        quote.setLowPrice(q.getLow().doubleValue());
        quote.setStartTime(startTime);
        quote.setEndTime(endTime);
        quote.setStock(q.getSymbol());
        quote.setVolume(q.getVolume());
        return quote;
    }

    private Interval getYahooInterval(INTERVAL interval) {
        if (interval == INTERVAL.WEEKLY) {
            return Interval.WEEKLY;
        } else {
            return Interval.DAILY;
        }
    }

    public List<Financials> getFinancials(Exchange exchange, String ticker) {
        List<Financials> financials = new ArrayList<Financials>();
        try {
            
            com.zavtech.morpheus.yahoo.YahooFinanceExt yahoo = new com.zavtech.morpheus.yahoo.YahooFinanceExt();
            DataFrame<LocalDate, YahooFieldExt> statistics = yahoo.getFinancials(ticker);
            Vector<LocalDate, YahooFieldExt> statsObj = statistics.data().rowCursor();
            for (int rowIndex = 0; rowIndex < statistics.rowCount(); rowIndex++) {
                statsObj.moveTo(rowIndex);
                com.nerds.stocks.data.Financials financial = new com.nerds.stocks.data.Financials();
                financial.setReportDate(statsObj.key());
                financial.setGrossProfit(getValue(statsObj, rowIndex, YahooFieldExt.GROSS_PROFIT));
                financial.setCostOfRevenue(getValue(statsObj, rowIndex, YahooFieldExt.COST_OF_REVENUE));
                financial.setOperatingRevenue(getValue(statsObj, rowIndex, YahooFieldExt.OPERATING_REVENUE));
                financial.setTotalRevenue(getValue(statsObj, rowIndex, YahooFieldExt.EARNINGS_TOTAL_REVENUE));
                financial.setOperatingIncome(getValue(statsObj, rowIndex, YahooFieldExt.OPERATING_INCOME));
                financial.setNetIncome(getValue(statsObj, rowIndex, YahooFieldExt.NET_INCOME));
                financial.setResearchAndDevelopment(getValue(statsObj, rowIndex, YahooFieldExt.RESEARCH_AND_DEVELOPMENT));
                financial.setOperatingExpense(getValue(statsObj, rowIndex, YahooFieldExt.OPERATING_EXPENSE));
                financial.setCurrentAssets(getValue(statsObj, rowIndex, YahooFieldExt.CURRENT_ASSETS));
                financial.setTotalAssets(getValue(statsObj, rowIndex, YahooFieldExt.TOTAL_ASSETS));
                financial.setTotalLiabilities(getValue(statsObj, rowIndex, YahooFieldExt.TOTAL_LIABILITIES));
                financial.setCurrentCash(getValue(statsObj, rowIndex, YahooFieldExt.CURRENT_CASH));
                financial.setCurrentDebt(getValue(statsObj, rowIndex, YahooFieldExt.CURRENT_DEBT));
                financial.setTotalCash(getValue(statsObj, rowIndex, YahooFieldExt.TOTAL_CASH));
                financial.setTotalDebt(getValue(statsObj, rowIndex, YahooFieldExt.TOTAL_DEBT));
                financial.setShareholderEquity(getValue(statsObj, rowIndex, YahooFieldExt.SHARE_HOLDER_EQUITY));
                financial.setCashChange(getValue(statsObj, rowIndex, YahooFieldExt.CASH_CHANGE));
                financial.setCashFlow(getValue(statsObj, rowIndex, YahooFieldExt.CASH_FLOW));
                financial.setOperatingGainsLosses(getValue(statsObj, rowIndex, YahooFieldExt.OPERATING_GAINS_LOSSES));
                financials.add(financial);
            }
        } catch(Exception e) {
            logger.error("Error trying to get financials for Yahoo", e);
        }
        return financials;
    }

    public void getEarnings() {

    }

    private List<Quote> getQuotes(QuotesResponse response) {
        List<Quote> quotes = new ArrayList<Quote>();

        if (response == null || response.getChart() == null || CollectionUtils.isEmpty(response.getChart().getResult())
                || response.getChart().getResult().get(0) == null
                || response.getChart().getResult().get(0).getIndicators() == null
                || CollectionUtils.isEmpty(response.getChart().getResult().get(0).getIndicators().getQuote())
                || response.getChart().getResult().get(0).getIndicators().getQuote().get(0) == null)
            return quotes;

        List<Long> times = response.getChart().getResult().get(0).getTimestamp();
        List<Double> closePrices = response.getChart().getResult().get(0).getIndicators().getQuote().get(0).getClose();
        List<Double> openPrices = response.getChart().getResult().get(0).getIndicators().getQuote().get(0).getOpen();
        List<Double> highPrices = response.getChart().getResult().get(0).getIndicators().getQuote().get(0).getHigh();
        List<Double> lowPrices = response.getChart().getResult().get(0).getIndicators().getQuote().get(0).getLow();
        List<Long> volumes = response.getChart().getResult().get(0).getIndicators().getQuote().get(0).getVolume();

        // long offset = 0;
        // if (response.getChart().getResult().get(0).getMeta().getGmtoffset() != null)
        // {
        // // offset in milli seconds
        // offset = 1000 *
        // response.getChart().getResult().get(0).getMeta().getGmtoffset();
        // }

        Timestamp startTimestamp = null;
        for (int counter = 0; counter < times.size(); counter++) {
            Timestamp endTimestamp = new Timestamp((times.get(counter) * 1000));
            if (startTimestamp == null) {
                startTimestamp = endTimestamp;
            }

            Quote quote = new Quote();
            quote.setStartTime(startTimestamp);
            quote.setEndTime(endTimestamp);

            if (closePrices.get(counter) != null) {
                quote.setClosePrice(closePrices.get(counter));
            }
            if (openPrices.get(counter) != null) {
                quote.setOpenPrice(openPrices.get(counter));
            }
            if (highPrices.get(counter) != null) {
                quote.setHighPrice(highPrices.get(counter));
            }
            if (lowPrices.get(counter) != null) {
                quote.setLowPrice(lowPrices.get(counter));
            }
            if (volumes.get(counter) != null) {
                quote.setVolume(volumes.get(counter));
            }

            if (quote.getClosePrice() != 0.0d && quote.getEndTime() != null) {
                quotes.add(quote);
            }

            startTimestamp = endTimestamp;
        }

        return quotes;
    }
}
