package com.nerds.stocks.service.api;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import org.patriques.AlphaVantageConnector;
import org.patriques.TimeSeries;
import org.patriques.input.timeseries.Interval;
import org.patriques.input.timeseries.OutputSize;
import org.patriques.output.timeseries.Daily;
import org.patriques.output.timeseries.IntraDay;
import org.patriques.output.timeseries.Weekly;
import org.patriques.output.timeseries.data.StockData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.nerds.stocks.data.Quote;
import com.nerds.stocks.service.data.INTERVAL;
import com.nerds.stocks.util.DateUtil;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter @Setter
@ConfigurationProperties(prefix = "stocks.keys")
public class AlphaVantageService {
    static final Logger logger = LoggerFactory.getLogger(AlphaVantageService.class);

    String alphavanatageApiKey;

    public void setAlphavanatageApiKey(String alphavanatageApiKey) {
        this.alphavanatageApiKey = alphavanatageApiKey;
    }

    public List<Quote> getPricesFrom(String ticker, Calendar from, INTERVAL givenInterval) {
        logger.info("Started loading prices for " + ticker + " from " + from);
        IntraDay intraDayResponse = null;
        Daily dailyResponse = null;
        Weekly weeklyResponse = null;
        List<StockData> stockData = new ArrayList<StockData>();
        AlphaVantageConnector apiConnector = new AlphaVantageConnector(alphavanatageApiKey, 3000);
        TimeSeries stockTimeSeries = new TimeSeries(apiConnector);
        List<Quote> prices = new ArrayList<Quote>();
        try {
            
            if(givenInterval == INTERVAL._5MIN) {
                intraDayResponse = stockTimeSeries.intraDay(ticker, Interval.FIVE_MIN, OutputSize.FULL);
                stockData = intraDayResponse.getStockData();
            } else if(givenInterval == INTERVAL._30MIN) {
                intraDayResponse = stockTimeSeries.intraDay(ticker, Interval.THIRTY_MIN, OutputSize.FULL);
                stockData = intraDayResponse.getStockData();
            } else if(givenInterval == INTERVAL.DAILY) {
                dailyResponse = stockTimeSeries.daily(ticker);
                stockData = dailyResponse.getStockData();
            } else {
                weeklyResponse = stockTimeSeries.weekly(ticker);
                stockData = weeklyResponse.getStockData();
            }

            LocalDateTime cutOffTime = DateUtil.getLocalDateTime(from);
            prices = stockData.parallelStream().filter(quote -> quote.getDateTime().compareTo(cutOffTime) > 0)
                    .map(data -> {
                        Quote quote = new Quote();
                        quote.setEndTime(Timestamp.valueOf(data.getDateTime()));
                        quote.setOpenPrice(data.getOpen());
                        quote.setHighPrice(data.getHigh());
                        quote.setLowPrice(data.getLow());
                        quote.setClosePrice(data.getClose());
                        quote.setVolume(data.getVolume());
                        return quote;
                    }).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Exception occurred trying to get stock prices for " + ticker + " from Alphavantage ", e);
        }
        logger.info("Got " + prices.size() + " records for " + ticker + " from " + from);
        return prices;
    }
    
}
