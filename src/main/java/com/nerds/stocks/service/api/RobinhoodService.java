package com.nerds.stocks.service.api;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.data.Stats;
import com.nerds.stocks.data.robinhood.FundamentalResult;
import com.nerds.stocks.data.robinhood.Historical;
import com.nerds.stocks.data.robinhood.HistoricalQuoteResponse;
import com.nerds.stocks.data.robinhood.InstrumentResponse;
import com.nerds.stocks.data.robinhood.QuoteResponse;
import com.nerds.stocks.service.cache.ExchangeMap;
import com.nerds.stocks.service.data.INTERVAL;
import com.nerds.stocks.service.data.TIMESERIES;

@Component
public class RobinhoodService {

    @Autowired
    ExchangeMap exchangesCache;

    public Fundamentals getFundamentals(String ticker) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.robinhood.com/fundamentals/" + ticker + "/";
        logger.info("Loading fundamentals for " + ticker + " using " + url);
        FundamentalResult result = restTemplate.getForObject(url, FundamentalResult.class);
        Fundamentals ret = new Fundamentals();
        ret.setSector(result.getSector());
        ret.setSymbol(ticker);
        ret.setIndustry(result.getIndustry());
        ret.setDescription(result.getDescription());
        ret.setRobinhoodId(
                result.getInstrument().replace("https://api.robinhood.com/instruments/", "").replace("/", ""));
        ret.setCity(result.getHeadquartersCity());
        ret.setState(result.getHeadquartersState());
        ret.setYearFounded(result.getYearFounded());

        ret.setStats(getStats(result));
        logger.debug("Fundamentals for " + ticker + " are \n " + ret);
        logger.info("Completed loading fundamentals for " + ticker);
        loadInstrumentInfo(ret);
        return ret;
    }

    public Stats getStats(String ticker) {
        RestTemplate restTemplate = new RestTemplate();
        FundamentalResult result = restTemplate.getForObject("https://api.robinhood.com/fundamentals/" + ticker + "/",
                FundamentalResult.class);

        return getStats(result);
    }

    private Stats getStats(FundamentalResult result) {
        Stats ret = new Stats();
        ret.setDividend(getBigDecimal(result.getDividendYield()));
        // ret.setAverageVolume(getDouble(result.getAverageVolume()));
        ret.setDividend(getBigDecimal(result.getDividendYield()));
        ret.setSharesOutstanding(getBigDecimal(result.getSharesOutstanding()));
        ret.setPe(getBigDecimal(result.getPeRatio()));
        // ret.setEps(getDouble()); // not available on robinhood

        return ret;
    }

    public List<Quote> getPrices(String ticker, INTERVAL interval) {
        List<Quote> quotes = new ArrayList<Quote>();
        String url = "https://api.robinhood.com/marketdata/historicals/" + ticker + "/?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("bounds", "regular");
        if (interval == INTERVAL._5MIN) {
            params.put("interval", "5minute");
            params.put("span", "week");
        } else if (interval == INTERVAL._30MIN) {
            params.put("interval", "hour");
            params.put("span", "month");
        } else if (interval == INTERVAL.DAILY) {
            params.put("interval", "day");
            params.put("span", "year");
        } else {
            params.put("interval", "week");
            params.put("span", "5year");
        }

        try {
            String paramStr = params.entrySet().stream()
                    .map(entry -> getEncodedRequestParameter(entry.getKey(), entry.getValue()))
                    .collect(Collectors.joining("&"));
            logger.info("Trying to get the prices from robinhood for " + ticker + " using " + url + paramStr);
            RestTemplate restTemplate = new RestTemplate();
            HistoricalQuoteResponse response = restTemplate.getForObject(url + paramStr, HistoricalQuoteResponse.class);
            List<Historical> historicals = response.getHistoricals();
            quotes = historicals.parallelStream().map(historical -> {
                Quote quote = new Quote();
                quote.setClosePrice(getDouble(historical.getClosePrice()));
                quote.setOpenPrice(getDouble(historical.getOpenPrice()));
                quote.setStartTime(getTimestamp(historical.getBeginsAt()));
                quote.setEndTime(getEndTime(quote.getStartTime(), interval));
                quote.setLowPrice(getDouble(historical.getLowPrice()));
                quote.setHighPrice(getDouble(historical.getHighPrice()));
                quote.setVolume(historical.getVolume());
                quote.setStock(response.getSymbol());
                return quote;
            }).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Error while trying to get historical quotes from robinhood", e);
        }

        return quotes;
    }

    public List<Quote> getCurrentPrices(String... tickers) {
        String symbols = String.join(",", tickers);
        String url = "https://api.robinhood.com/quotes/?symbols=" + symbols;
        logger.debug("Started loading current prices for " + symbols + " using " + url);
        RestTemplate restTemplate = new RestTemplate();
        QuoteResponse responseFromRobinhood = restTemplate.getForObject(url, QuoteResponse.class);
        return responseFromRobinhood.getResults().parallelStream().map(result -> {
            Quote quote = new Quote();
            quote.setClosePrice(getDouble(result.getLastTradePrice()));
            quote.setOpenPrice(getDouble(result.getAdjustedPreviousClose()));
            quote.setEndTime(getTimestamp(result.getUpdatedAt()));
            quote.setStartTime(getTimestamp(result.getPreviousCloseDate()));
            quote.setVolume(-1);
            quote.setStock(result.getSymbol());

            return quote;
        }).collect(Collectors.toList());
    }

    public void loadInstrumentInfo(Fundamentals fundamentals) {
        String url = "https://api.robinhood.com/instruments/" + fundamentals.getRobinhoodId() + "/";
        logger.debug("Started loading instrument values using " + url);
        RestTemplate restTemplate = new RestTemplate();
        InstrumentResponse result = restTemplate.getForObject(url, InstrumentResponse.class);
        fundamentals.setName(result.getName());
        fundamentals.setMarginInitialRatio(getDouble(result.getMarginInitialRatio()));
        fundamentals.setMaintenanceRatio(getDouble(result.getMaintenanceRatio()));
        fundamentals.setExchange(exchangesCache.getRobinhoodExchange(result.getMarket()));
    }

    public Double getDouble(String value) {
        double retValue = 0.0;
        if (value != null) {
            try {
                retValue = Double.parseDouble(value);
            } catch (Exception e) {

            }
        }
        return retValue;
    }

    public Long getLong(String value) {
        long retValue = 0;
        if (value != null) {
            try {
                retValue = Long.parseLong(value);
            } catch (Exception e) {

            }
        }
        return retValue;
    }

    public BigDecimal getBigDecimal(String value) {
        return BigDecimal.valueOf(getDouble(value));
    }

    public Timestamp getTimestamp(String val) {
        Timestamp ret = null;

        if (val != null) {
            try {
                ret = new Timestamp(DatatypeConverter.parseDateTime(val).getTimeInMillis());
            } catch (Exception e) {
                logger.error("Error parsing timestamp in robinhood ");
            }
        }

        return ret;
    }

    public double getDividedValue(double quotition, double divider) {
        double ret = 0;
        try {
            ret = quotition / divider;
        } catch (Exception e) {

        }
        return ret;
    }

    public String getEncodedRequestParameter(String key, String value) {
        try {
            return URLEncoder.encode(key, "utf-8") + "=" + URLEncoder.encode(value, "utf-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("Error while enconding request parameter in robinhood ==> " + key + ", value = " + value);
            return key + "=" + value;
        }
    }

    public String getRobinhoodInterval(INTERVAL givenInterval) {
        if (givenInterval == INTERVAL._5MIN) {
            return "5minute";
        } else if (givenInterval == INTERVAL._30MIN) {
            return "hour";
        } else if (givenInterval == INTERVAL.DAILY) {
            return "day";
        } else {
            return "week";
        }

    }

    public String getRobinhoodSpan(TIMESERIES timeSeries) {
        if (timeSeries == TIMESERIES.ONE_DAY) {
            return "day";
        } else if (timeSeries == TIMESERIES.FIVE_DAY) {
            return "week";
        } else if (timeSeries == TIMESERIES.ONE_MONTH) {
            return "month";
        } else if (timeSeries == TIMESERIES.SIX_MONTH || timeSeries == TIMESERIES.ONE_YEAR) {
            return "year";
        } else if (timeSeries == TIMESERIES.FIVE_YEAR) {
            return "5year";
        } else {
            return "5year";
        }
    }

    public Timestamp getEndTime(Timestamp startTime, INTERVAL givenInterval) {
        long startTimeInMillis = startTime.getTime();
        if (givenInterval == INTERVAL._5MIN) {
            startTimeInMillis = startTimeInMillis + 300000l;
        } else if (givenInterval == INTERVAL._30MIN) {
            startTimeInMillis = startTimeInMillis + 1800000l;
        } else if (givenInterval == INTERVAL.DAILY) {
            startTimeInMillis = startTimeInMillis + (24 * 3600000l);
        } else {
            startTimeInMillis = startTimeInMillis + (148 * 3600000l);
        }

        return new Timestamp(startTimeInMillis);
    }

    static final Logger logger = LoggerFactory.getLogger(RobinhoodService.class);
}