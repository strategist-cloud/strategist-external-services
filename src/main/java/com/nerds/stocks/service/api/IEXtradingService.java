package com.nerds.stocks.service.api;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Stats;
import com.nerds.stocks.service.cache.ExchangeMap;
import com.nerds.stocks.service.data.INTERVAL;
import com.nerds.stocks.util.DateUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import pl.zankowski.iextrading4j.api.stocks.Chart;
import pl.zankowski.iextrading4j.api.stocks.ChartRange;
import pl.zankowski.iextrading4j.api.stocks.Company;
import pl.zankowski.iextrading4j.api.stocks.Earning;
import pl.zankowski.iextrading4j.api.stocks.Earnings;
import pl.zankowski.iextrading4j.api.stocks.Quote;
import pl.zankowski.iextrading4j.api.stocks.v1.AdvancedStats;
import pl.zankowski.iextrading4j.api.stocks.v1.Financial;
import pl.zankowski.iextrading4j.api.stocks.v1.Financials;
import pl.zankowski.iextrading4j.client.IEXCloudClient;
import pl.zankowski.iextrading4j.client.IEXCloudTokenBuilder;
import pl.zankowski.iextrading4j.client.IEXTradingApiVersion;
import pl.zankowski.iextrading4j.client.IEXTradingClient;
import pl.zankowski.iextrading4j.client.rest.request.stocks.ChartRequestBuilder;
import pl.zankowski.iextrading4j.client.rest.request.stocks.CompanyRequestBuilder;
import pl.zankowski.iextrading4j.client.rest.request.stocks.EarningsRequestBuilder;
import pl.zankowski.iextrading4j.client.rest.request.stocks.QuoteRequestBuilder;
import pl.zankowski.iextrading4j.client.rest.request.stocks.v1.AdvancedStatsRequestBuilder;
import pl.zankowski.iextrading4j.client.rest.request.stocks.v1.FinancialsRequestBuilder;

@Component
public class IEXtradingService {
    @Autowired
    ExchangeMap exchangesSymbolsMap;

    private final IEXCloudClient iexTradingClient = IEXTradingClient.create(IEXTradingApiVersion.IEX_CLOUD_V1,
            new IEXCloudTokenBuilder().withPublishableToken("pk_484a28e5c8d44d5eae946d9de652e1e8")
                    .withSecretToken("sk_1fdb29670eb64853a75db20eba320cbc").build());

    static final Logger logger = LoggerFactory.getLogger(IEXtradingService.class);

    public Fundamentals getFundamentals(String ticker) {
        Fundamentals ret = null;
        logger.info("Loading fundamentals for " + ticker + " from IEX Trading.");
        try {
            final Company result = iexTradingClient
                    .executeRequest(new CompanyRequestBuilder().withSymbol(ticker).build());
            if (result == null || result.getCompanyName() == null) {
                logger.info("***IEX: No fundamentals info found for " + ticker);
                return null;
            } else {
                ret = getStockInfo(result);

                logger.debug("Fundamentals for " + ticker + " are \n " + ret);
                logger.info("Completed loading fundamentals for " + ticker);

                return ret;
            }
        } catch (Exception e) {
            logger.error("Unable to load fundamentals from IEX");
        }
        return ret;
    }

    public Stats getStats(String ticker) {
        Stats stats = null;
        try {
            AdvancedStats keyStats = iexTradingClient
                    .executeRequest(new AdvancedStatsRequestBuilder().withSymbol(ticker).build());
            if (keyStats != null) {
                stats = getStats(keyStats);
            }
        } catch (Exception e) {
            logger.error("Error on loading stats for " + ticker);
        }
        return stats;
    }

    // public Fundamentals initStock(String ticker) {
    // logger.info("****** IEX initiating stock for " + ticker);
    // Fundamentals stock = new Fundamentals();
    // try {
    // Company companyInfo = iexTradingClient
    // .executeRequest(new CompanyRequestBuilder().withSymbol(ticker).build());
    // if (companyInfo != null) {
    // stock = getStockInfo(companyInfo);
    // stock.setCurrentPrice(getQuote(ticker));
    // stock.setFinancials(getFinancials(ticker));
    // stock.setEarnings(getEarnings(ticker));
    // stock.setStats(getStats(ticker));
    // }
    // } catch (Exception e) {
    // logger.error("Error while trying to init stock using IEX", e);
    // }

    // return stock;
    // }

    private Fundamentals getStockInfo(Company result) {
        Fundamentals ret = new Fundamentals();
        ret.setSector(result.getSector());
        ret.setSymbol(result.getSymbol());
        ret.setIndustry(result.getIndustry());
        ret.setDescription(result.getDescription());
        ret.setExchange(exchangesSymbolsMap.getIexExchange(result.getExchange()));
        ret.setName(result.getCompanyName());
        // ret.setRobinhoodId(result.getInstrument().replace("https://api.robinhood.com/instruments/",
        // "").replace("/", ""));
        // ret.setCity(result.getHeadquartersCity());
        // ret.setState(result.getHeadquartersState());
        // ret.setYearFounded(result.getYearFounded());

        return ret;
    }

    // private Stats getAdvancedStats(AdvancedStats keyStats) {

    // keyStats.getAvg10Volume();
    // keyStats.getAvg30Volume();
    // keyStats.getBeta();
    // keyStats.getCompanyName();
    // keyStats.getCurrentDebt();
    // keyStats.getDebtToEquity();
    // keyStats.getDividendYield();
    // keyStats.getEbitda();
    // keyStats.getEmployees();
    // keyStats.getEnterpriseValue();
    // keyStats.getEnterpriseValueToRevenue();
    // keyStats.getExDividendDate();
    // keyStats.getFloat();
    // keyStats.getForwardPERatio();
    // keyStats.getGrossProfit();
    // keyStats.getMarketcap();
    // keyStats.getMaxChangePercent();
    // keyStats.getNextDividendDate();
    // keyStats.getNextEarningsDate();
    // keyStats.getPeRatio();
    // keyStats.getPegRatio();
    // keyStats.getPriceToBook();
    // keyStats.getPriceToSales();
    // keyStats.getProfitMargin();
    // keyStats.getRevenue();
    // keyStats.getRevenuePerEmployee();
    // keyStats.getRevenuePerShare();
    // keyStats.getSharesOutstanding();
    // keyStats.getSymbol();
    // keyStats.getTotalCash();
    // keyStats.getTotalRevenue();
    // keyStats.getTtmDividendRate();
    // keyStats.getTtmEPS();
    // keyStats.getWeek52change();
    // keyStats.getWeek52high();
    // keyStats.getWeek52low();
    // keyStats.getYear1ChangePercent();
    // keyStats.getYear2ChangePercent();
    // keyStats.getYear5ChangePercent();
    // keyStats.getYtdChangePercent();
    // keyStats.getDay200MovingAvg();
    // keyStats.getDay30ChangePercent();
    // keyStats.getDay50MovingAvg();
    // keyStats.getDay5ChangePercent();
    // keyStats.getMonth1ChangePercent();
    // keyStats.getMonth3ChangePercent();
    // keyStats.getMonth6ChangePercent();

    // }

    private Stats getStats(AdvancedStats keyStats) {
        Stats ret = new Stats();

        ret.setBookValuePerShare(keyStats.getPriceToBook());
        ret.setDividend(keyStats.getDividendYield());
        ret.setEBITDA(keyStats.getEbitda());
        ret.setEps(keyStats.getTtmEPS());
        ret.setPe(keyStats.getPeRatio());
        ret.setPeg(keyStats.getPegRatio());
        ret.setSharesFloat(keyStats.getFloat());
        ret.setSharesOutstanding(keyStats.getSharesOutstanding());
        // ret.setEarningsAnnouncement(DateUtil.getCalendar(keyStats.getNextEarningsDate()));
        ret.setPriceBook(keyStats.getPriceToBook());
        ret.setPriceSales(keyStats.getPriceToSales());
        ret.setRevenue(keyStats.getRevenue());
        // ret.setShortRatio(keyStats.getShortRatio());
        // ret.setEpsEstimateCurrentYear(keyStats.getEpsEstimateCurrentYear());
        // ret.setEpsEstimateNextQuarter(keyStats.getEpsEstimateNextQuarter());
        // ret.setEpsEstimateNextYear(keyStats.getEpsEstimateNextYear());
        // ret.setOneYearTargetPrice(keyStats.getOneYearTargetPrice());
        // ret.setSharesOwned(keyStats.getS);
        return ret;
    }

    // public List<com.nerds.stocks.data.Quote> getQuotes(String... tickers) {
    // if (tickers == null)
    // return new ArrayList<com.nerds.stocks.data.Quote>();
    // return
    // Arrays.asList(tickers).parallelStream().map(this::getQuote).collect(Collectors.toList());
    // }

    public com.nerds.stocks.data.Quote getQuote(String ticker) {
        logger.info("Trying to get quote for " + ticker + " from IEX");
        com.nerds.stocks.data.Quote quote = null;
        try {
            final Quote iexQuote = iexTradingClient
                    .executeRequest(new QuoteRequestBuilder().withSymbol(ticker).build());
            logger.info("Loaded quote for " + ticker + " from IEX");
            quote = getQuote(iexQuote);
        } catch (Exception e) {
            logger.error("Error while loading quote from IEX platform");
        }
        return quote;
    }

    public List<com.nerds.stocks.data.Quote> getHistoricalQuotes(Exchange exchange, String ticker, INTERVAL interval) {
        logger.info("**** IEX, Trying to get " + interval + " historical quotes for " + ticker);
        List<com.nerds.stocks.data.Quote> quotes = new ArrayList<com.nerds.stocks.data.Quote>();
        try {
            if (ticker == null || interval == null) {
                logger.info("Either ticker or interval info is not given. Ticker = " + ticker + ", interval = " + interval);
                return quotes;
            }
            ChartRequestBuilder builder = new ChartRequestBuilder().withSymbol(ticker);
            if (interval == INTERVAL._30MIN || interval == INTERVAL._5MIN) {
                List<LocalDate> dates = new ArrayList<LocalDate>();
                LocalDate currentDate = LocalDate.now();

                final int minutes;
                if (interval == INTERVAL._5MIN) {
                    minutes = 5;
                    dates.add(DateUtil.getLocalDateTime(exchange.getStartingTimeOfLastTradingDay()).toLocalDate());
                } else {
                    minutes = 30;
                    dates.add(currentDate);
                    for (int i = 0; i < 6; i++) { // get 7 days of data for 30 min intervals
                        currentDate = currentDate.minusDays(1);
                        dates.add(currentDate);
                    }
                }

                quotes = dates.parallelStream().map(date -> {
                    logger.info("Trying to get historical quotes for " + ticker + " on " + date);
                    return iexTradingClient.executeRequest(builder.withDate(date).withChartInterval(minutes).build());
                }).flatMap(List::stream).filter(chart -> !StringUtils.isEmpty(chart.getClose()))
                        .map(chartInfo -> getQuote(chartInfo, interval, ticker)).filter(quote -> quote != null)
                        // remove the quotes with no start time or end time from iex
                        // .distinct()
                        .collect(Collectors.toList());
            } else {
                if (interval == INTERVAL.DAILY) {
                    builder.withChartRange(ChartRange.ONE_YEAR);
                } else if (interval == INTERVAL.WEEKLY) {
                    builder.withChartRange(ChartRange.FIVE_YEARS);
                }

                quotes = iexTradingClient.executeRequest(builder.build()).parallelStream()
                        .map(chart -> getQuote(chart, interval, ticker)).filter(quote -> quote != null)
                        // remove the quotes with no start time or end time from iex
                        .collect(Collectors.toList());
            }
            logger.info("******* IEX, Completed loading " + interval + " historical quotes for " + ticker + ". Total = "
                    + quotes.size());
        }catch (Exception e) {
            logger.error("Error while loading Historical quotes from IEX", e);
        }
        return quotes;
    }

    private static final ZoneId easternZoneId = ZoneId.of("America/New_York");
    private static final DateTimeFormatter closeTimeFormatInChart = DateTimeFormatter.ofPattern("yyyyMMddHH:mm")
            .withZone(easternZoneId);

    private com.nerds.stocks.data.Quote getQuote(Chart iexQuote, INTERVAL interval, String ticker) {
        com.nerds.stocks.data.Quote quote = new com.nerds.stocks.data.Quote();
        quote.setClosePrice(iexQuote.getClose().doubleValue());
        if (iexQuote.getDate() != null && iexQuote.getMinute() != null) {
            LocalDateTime closeTime = LocalDateTime.parse(iexQuote.getDate() + "" + iexQuote.getMinute(),
                    closeTimeFormatInChart);
            closeTime = closeTime.atZone(easternZoneId).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
            logger.info("Close Time " + closeTime);
            // ZonedDateTime closeTimeInEasternZone = ZonedDateTime.of(closeTime,
            // easternZoneId);
            quote.setEndTime(DateUtil.getTimestamp(closeTime));
            logger.info("Close Timestamp " + quote.getEndTime());
            if (interval != null)
                quote.setStartTime(DateUtil.getStartTime(closeTime, interval));
        } else {
            // Since the start time and end time can not be calculated, we are returning
            // null
            return null;
        }
        quote.setLowPrice(iexQuote.getLow().doubleValue());
        quote.setOpenPrice(iexQuote.getOpen().doubleValue());
        quote.setStock(ticker);
        quote.setVolume(iexQuote.getVolume().longValue());
        return quote;
    }

    private com.nerds.stocks.data.Quote getQuote(Quote iexQuote) {
        com.nerds.stocks.data.Quote quote = new com.nerds.stocks.data.Quote();
        quote.setClosePrice(iexQuote.getClose().doubleValue());
        quote.setEndTime(new Timestamp(iexQuote.getClose().longValue()));
        quote.setHighPrice(iexQuote.getHigh().doubleValue());
        quote.setLowPrice(iexQuote.getLow().doubleValue());
        quote.setOpenPrice(iexQuote.getOpen().doubleValue());
        quote.setStartTime(new Timestamp(iexQuote.getOpenTime().longValue()));
        quote.setStock(iexQuote.getSymbol());
        quote.setVolume(iexQuote.getLatestVolume().longValue());
        return quote;
    }

    public List<com.nerds.stocks.data.Financials> getFinancials(String ticker) {
        List<com.nerds.stocks.data.Financials> financialData =new ArrayList<com.nerds.stocks.data.Financials>();
        try {
            Financials financials = iexTradingClient
                    .executeRequest(new FinancialsRequestBuilder().withSymbol(ticker).build());
            financialData = getFinancials(financials);
        }catch (Exception e) {
            logger.error("Error while trying to get financials from IEX", e);
        }
        return financialData;
    }

    private List<com.nerds.stocks.data.Financials> getFinancials(Financials iexFinancials) {
        if (iexFinancials != null) {
            return iexFinancials.getFinancials().parallelStream().map(financial -> getFinancial(financial))
                    .collect(Collectors.toList());
        }
        return new ArrayList<com.nerds.stocks.data.Financials>();
    }

    private com.nerds.stocks.data.Financials getFinancial(Financial iexFinancial) {
        com.nerds.stocks.data.Financials financials = new com.nerds.stocks.data.Financials();

        financials.setReportDate(iexFinancial.getReportDate());
        financials.setGrossProfit(getDouble(iexFinancial.getGrossProfit()));
        financials.setCostOfRevenue(getDouble(iexFinancial.getCostOfRevenue()));
        financials.setOperatingRevenue(getDouble(iexFinancial.getOperatingRevenue()));
        financials.setTotalRevenue(getDouble(iexFinancial.getTotalRevenue()));
        financials.setOperatingIncome(getDouble(iexFinancial.getOperatingIncome()));
        financials.setNetIncome(getDouble(iexFinancial.getNetIncome()));
        financials.setResearchAndDevelopment(getDouble(iexFinancial.getResearchAndDevelopment()));
        financials.setOperatingExpense(getDouble(iexFinancial.getOperatingExpense()));
        financials.setCurrentAssets(getDouble(iexFinancial.getCurrentAssets()));
        financials.setTotalAssets(getDouble(iexFinancial.getTotalAssets()));
        financials.setTotalLiabilities(getDouble(iexFinancial.getTotalLiabilities()));
        financials.setCurrentCash(getDouble(iexFinancial.getCurrentCash()));
        financials.setCurrentDebt(getDouble(iexFinancial.getCurrentDebt()));
        financials.setTotalCash(getDouble(iexFinancial.getTotalCash()));
        financials.setTotalDebt(getDouble(iexFinancial.getTotalDebt()));
        financials.setShareholderEquity(getDouble(iexFinancial.getShareholderEquity()));
        financials.setCashChange(getDouble(iexFinancial.getCashChange()));
        financials.setCashFlow(getDouble(iexFinancial.getCashFlow()));
        // financials.setOperatingGainsLosses(iexFinancial.getOperatingGainsLosses());

        return financials;
    }

    public List<com.nerds.stocks.data.Earnings> getEarnings(String ticker) {
        final Earnings earnings = iexTradingClient
                .executeRequest(new EarningsRequestBuilder().withSymbol(ticker).build());
        return getEarningsList(earnings);
    }

    private List<com.nerds.stocks.data.Earnings> getEarningsList(
            pl.zankowski.iextrading4j.api.stocks.Earnings iexEarnings) {
        if (iexEarnings != null) {
            return iexEarnings.getEarnings().parallelStream().map(earnings -> getEarnings(earnings))
                    .collect(Collectors.toList());
        }
        return new ArrayList<com.nerds.stocks.data.Earnings>();
    }

    private com.nerds.stocks.data.Earnings getEarnings(Earning iexEarnings) {
        com.nerds.stocks.data.Earnings earnings = new com.nerds.stocks.data.Earnings();
        earnings.setActualEPS(iexEarnings.getActualEPS());
        earnings.setConsensusEPS(iexEarnings.getConsensusEPS());
        earnings.setEstimatedEPS(iexEarnings.getConsensusEPS());
        earnings.setAnnounceTime(iexEarnings.getAnnounceTime());
        earnings.setNumberOfEstimates(iexEarnings.getNumberOfEstimates());
        earnings.setEPSSurpriseDollar(iexEarnings.getEPSSurpriseDollar());
        earnings.setFiscalPeriod(iexEarnings.getFiscalPeriod());
        earnings.setYearAgo(iexEarnings.getYearAgo());

        return earnings;
    }

    public Double getDouble(BigDecimal value) {
        if (value == null)
            return null;
        return value.doubleValue();
    }

    // private void batchRequestSample() {
    // final BatchStocks batchStocks = iexTradingClient.executeRequest(new
    // BatchStocksRequestBuilder()
    // .withSymbol("AAPL")
    // .addType(BatchStocksType.BOOK)
    // .addType(BatchStocksType.COMPANY)
    // .addType(BatchStocksType.EARNINGS)
    // .build());
    // System.out.println(batchStocks);
    // }

    // private void batchMarketRequestSample() {
    // final Map<String, BatchStocks> batchStocksMap =
    // iexTradingClient.executeRequest(new BatchMarketStocksRequestBuilder()
    // .withSymbol("AAPL")
    // .addType(BatchStocksType.COMPANY)
    // .addType(BatchStocksType.EARNINGS)
    // .build());
    // System.out.println(batchStocksMap);
    // }

    // private void bookRequestSample() {
    // final Book book = iexTradingClient.executeRequest(new BookRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(book);
    // }

    // private void cryptoRequestSample() {
    // final List<Quote> cryptoResponse = iexTradingClient.executeRequest(new
    // CryptoRequestBuilder()
    // .build());
    // System.out.println(cryptoResponse);
    // }

    // private void chartRequestSample() {
    // final List<Chart> chartList = iexTradingClient.executeRequest(new
    // ChartRequestBuilder()
    // .withChartRange(ChartRange.ONE_MONTH)
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(chartList);
    // }

    // private void dynamicChartRequestSample() {
    // final DynamicChart dynamicChart = iexTradingClient.executeRequest(new
    // DynamicChartRequestBuilder()
    // .withSymbol("aapl")
    // .build());
    // System.out.println(dynamicChart);
    // }

    // private void companyRequestSample() {
    // final Company company = iexTradingClient.executeRequest(new
    // CompanyRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(company);
    // }

    // private void delayedQuoteRequestSample() {
    // final DelayedQuote delayedQuote = iexTradingClient.executeRequest(new
    // DelayedQuoteRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(delayedQuote);
    // }

    // private void dividendsRequestSample() {
    // final List<Dividends> dividends = iexTradingClient.executeRequest(new
    // DividendsRequestBuilder()
    // .withSymbol("AAPL")
    // .withDividendRange(DividendRange.SIX_MONTHS)
    // .build());
    // System.out.println(dividends);
    // }

    // private void earningsRequestSample() {
    // final Earnings earnings = iexTradingClient.executeRequest(new
    // EarningsRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(earnings);
    // }

    // private void todayEarningsRequestSample() {
    // final TodayEarnings todayEarnings = iexTradingClient.executeRequest(new
    // TodayEarningsRequestBuilder()
    // .build());
    // System.out.println(todayEarnings);
    // }

    // private void effectiveSpreadRequestSample() {
    // final List<EffectiveSpread> effectiveSpreads =
    // iexTradingClient.executeRequest(new EffectiveSpreadRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(effectiveSpreads);
    // }

    // private void financialsRequestSample() {
    // final Financials financials = iexTradingClient.executeRequest(new
    // FinancialsRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(financials);
    // }

    // private void keyStatsRequestSample() {
    // final KeyStats keyStats = iexTradingClient.executeRequest(new
    // KeyStatsRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(keyStats);
    // }

    // private void listRequestSample() {
    // final List<Quote> quoteList = iexTradingClient.executeRequest(new
    // ListRequestBuilder()
    // .withListType(ListType.IEXVOLUME)
    // .build());
    // System.out.println(quoteList);
    // }

    // private void logoRequestSample() {
    // final Logo logo = iexTradingClient.executeRequest(new LogoRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(logo);
    // }

    // private void newsRequestSample() {
    // final List<News> newsList = iexTradingClient.executeRequest(new
    // NewsRequestBuilder()
    // .withWorldNews()
    // .build());
    // System.out.println(newsList);
    // }

    // private void openCloseRequestSample() {
    // final Ohlc ohlc = iexTradingClient.executeRequest(new
    // OpenCloseRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(ohlc);
    // }

    // private void peersRequestSample() {
    // final List<String> peers = iexTradingClient.executeRequest(new
    // PeersRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(peers);
    // }

    // private void previousRequestSample() {
    // final BarData bar = iexTradingClient.executeRequest(new
    // PreviousRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(bar);
    // }

    // private void previousMarketRequestSample() {
    // final Map<String, BarData> barDataMap = iexTradingClient.executeRequest(new
    // PreviousMarketRequestBuilder()
    // .build());
    // System.out.println(barDataMap);
    // }

    // private void priceRequestSample() {
    // final BigDecimal price = iexTradingClient.executeRequest(new
    // PriceRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(price);
    // }

    // private void quoteRequestSample() {
    // final List<ExchangeSymbol> exchangeSymbols =
    // iexTradingClient.executeRequest(new SymbolsRequestBuilder().build());
    // for (final ExchangeSymbol exchangeSymbol : exchangeSymbols) {
    // final Quote quote = iexTradingClient.executeRequest(new QuoteRequestBuilder()
    // .withSymbol(exchangeSymbol.getSymbol())
    // .build());

    // System.out.println(quote);
    // }
    // }

    // private void relevantRequestSample() {
    // final Relevant relevant = iexTradingClient.executeRequest(new
    // RelevantRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(relevant);
    // }

    // private void splitsRequestSample() {
    // final List<Split> splitList = iexTradingClient.executeRequest(new
    // SplitsRequestBuilder()
    // .withSymbol("AAPL")
    // .withSplitsRange(SplitsRange.FIVE_YEARS)
    // .build());
    // System.out.println(splitList);
    // }

    // private void venueVolumeRequestSample() {
    // final List<VenueVolume> venueVolumeList = iexTradingClient.executeRequest(new
    // VenueVolumeRequestBuilder()
    // .withSymbol("AAPL")
    // .build());
    // System.out.println(venueVolumeList);
    // }

    // private void ohlcRequestSample() {
    // final Ohlc ohlc = iexTradingClient.executeRequest(new OhlcRequestBuilder()
    // .withSymbol("aapl")
    // .build());
    // System.out.println(ohlc);
    // }

    // private void ohlcMarketRequestSample() {
    // final Map<String, Ohlc> ohlcMap = iexTradingClient.executeRequest(new
    // OhlcMarketRequestBuilder()
    // .build());
    // System.out.println(ohlcMap);
    // }

    // private void shortInterestRequestSample() {
    // final List<ShortInterest> shortInterestList =
    // iexTradingClient.executeRequest(new ShortInterestRequestBuilder()
    // .withSample()
    // .withMarket()
    // .build());
    // System.out.println(shortInterestList);
    // }

    // private void timeSeriesRequestSample() {
    // final List<TimeSeries> timeSeriesList = iexTradingClient.executeRequest(new
    // TimeSeriesRequestBuilder()
    // .withSymbol("aapl")
    // .build());
    // System.out.println(timeSeriesList);
    // }

    // private void thresholdSecuritiesRequestSample() {
    // final List<ThresholdSecurities> thresholdSecuritiesList =
    // iexTradingClient.executeRequest(new ThresholdSecuritiesRequestBuilder()
    // .withSample()
    // .withMarket()
    // .build());
    // System.out.println(thresholdSecuritiesList);
    // }

    // private void todayIposRequestSample() {
    // final TodayIpos todayIpos = iexTradingClient.executeRequest(new
    // TodayIposRequestBuilder()
    // .build());
    // System.out.println(todayIpos);
    // }

    // private void upcomingIposRequestSample() {
    // final Ipos ipos = iexTradingClient.executeRequest(new
    // UpcomingIposRequestBuilder()
    // .build());
    // System.out.println(ipos);
    // }

    // private void sectorPerformanceRequestSample() {
    // final List<SectorPerformance> sectorPerformances =
    // iexTradingClient.executeRequest(
    // new SectorPerformanceRequestBuilder().build());
    // System.out.println(sectorPerformances);
    // }

    // private void sectorCollectionRequestSample() {
    // final List<Quote> quotes = iexTradingClient.executeRequest(new
    // CollectionRequestBuilder()
    // .withCollectionType(CollectionType.SECTOR)
    // .withCollectionName("Financials")
    // .build());
    // System.out.println(quotes);
    // }

    // private void listCollectionRequestSample() {
    // final List<Quote> quotes = iexTradingClient.executeRequest(new
    // CollectionRequestBuilder()
    // .withCollectionType(CollectionType.LIST)
    // .withCollectionName("iexvolume")
    // .build());
    // System.out.println(quotes);
    // }

    // private void tagCollectionRequestSample() {
    // final List<Quote> quotes = iexTradingClient.executeRequest(new
    // CollectionRequestBuilder()
    // .withCollectionType(CollectionType.TAG)
    // .withCollectionName("Technology")
    // .build());
    // System.out.println(quotes);
    // }

}