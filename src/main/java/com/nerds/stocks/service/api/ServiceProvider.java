package com.nerds.stocks.service.api;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Financials;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.data.Stats;
import com.nerds.stocks.service.cache.ExchangeMap;
import com.nerds.stocks.service.data.INTERVAL;
import com.nerds.stocks.service.data.TIMESERIES;
import com.nerds.stocks.service.util.TimeUtil;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ServiceProvider {
    @Autowired
    RobinhoodService robinhood;
    @Autowired
    YahooStockService yahoo;
    @Autowired
    AlphaVantageService alpha;
    @Autowired
    IEXtradingService iexTrading;
    @Autowired
    ExchangeMap exchangeCache;

    public Fundamentals getFundamentals(String exchangeId, String ticker) {
        logger.debug("Trying to get fundamentals for " + exchangeId + "/" + ticker);
        Fundamentals fundamentals = iexTrading.getFundamentals(ticker);
        Exchange exchange = exchangeCache.getExchange(exchangeId);

        if (fundamentals == null || StringUtils.isEmpty(fundamentals.getName())) {
            logger.warn("Unable to get fundamentals from Iex Trading for " + exchangeId + "/" + ticker);
            fundamentals = yahoo.getFundamentals(exchange, ticker, fundamentals);

            if (fundamentals == null || StringUtils.isEmpty(fundamentals.getName())) {
                logger.warn("Unable to get fundamentals from yahoo for " + exchangeId + "/" + ticker);
                fundamentals = robinhood.getFundamentals(ticker);

                if(fundamentals == null || StringUtils.isEmpty(fundamentals.getName())) {
                    logger.warn("Unable to get fundamentals from robinhood for " + exchangeId + "/" + ticker + ". Fundamentals Query failed");
                }else {
                    logger.info("Got Fundamentals for " + exchangeId + "/" + ticker + " from robinhood.");
                }
            } else {
                logger.info("Got Fundamentals for " + exchangeId + "/" + ticker + " from yahoo.");
            }
        }else {
            logger.info("Got Fundamentals for " + exchangeId + "/" + ticker + " from iexTrading.");
        }

        logger.debug("Fundamentals = " + fundamentals);
        fundamentals.setCurrentPrice(getCurrentPrice(exchangeId, ticker));
        fundamentals.setStats(getStats(exchangeId, ticker));

        return fundamentals;
    }

    // public List<Quote> getCurrentPrices(String... tickers) {
    // List<Quote> quotes = new ArrayList<Quote>();

    // quotes = iexTrading.getQuotes(tickers);

    // return quotes;
    // }

    public Quote getCurrentPrice(String exchangeId, String ticker) {
        logger.info("Trying to get current price for " + exchangeId + "/" + ticker);
        Quote quote = iexTrading.getQuote(ticker);
        
        if (quote == null || quote.getClosePrice() == 0.0d || quote.getEndTime() == null) {
            logger.info("Unable to get current price for " + exchangeId + "/" + ticker + " from iex trading.");
            Exchange exchange = exchangeCache.getExchange(exchangeId);
            quote = yahoo.getCurrentPrice(exchange, ticker);

            if (quote == null || quote.getClosePrice() == 0.0d || quote.getEndTime() == null) {
                logger.info("Unable to get current price for " + exchangeId + "/" + ticker + " from yahoo.");
                List<Quote> quotes = robinhood.getCurrentPrices(ticker);
                if (CollectionUtils.isNotEmpty(quotes)) {
                    logger.info("Got current price for " + exchangeId + "/" + ticker + " from robinhood.");
                    quote = quotes.get(0);
                } else {
                    logger.info("Unable to get current price for " + exchangeId + "/" + ticker + " from robinhood.");
                }
            } else {
                logger.info("Got current price for " + exchangeId + "/" + ticker + " from yahoo.");
            }
        } else {
            logger.info("Got current price for " + exchangeId + "/" + ticker + " from iexTrading.");
        }

        logger.debug("Latest quote for " + exchangeId + "/" + ticker + " is " + quote);
        return quote;
    }

    public List<Quote> getPrices(String exchangeId, String ticker, INTERVAL interval) {
        Exchange exchange = exchangeCache.getExchange(exchangeId);
        logger.info("Started loading historical prices of " + exchangeId + "/" + ticker + ", for " + interval + " time period");

        TIMESERIES timeSeries = TimeUtil.getTimeseries(interval);
        Calendar fromTime = TimeUtil.getTimestamp(timeSeries, exchange);
        logger.debug("Calcualted Time series and From time for given " + exchangeId + "/" + ticker + "-" + interval 
            + " are " + timeSeries + ", " + fromTime);

        List<Quote> prices = yahoo.getPricesFrom(exchange, ticker, fromTime, interval);
        if (CollectionUtils.isEmpty(prices)) {
            logger.info("Unabelt to get historical prices for " + exchangeId + "/" + ticker + "-" + interval + " from yahoo.");
            prices = iexTrading.getHistoricalQuotes(exchange, ticker, interval);
            if (CollectionUtils.isEmpty(prices)) {
                logger.info("Unabelt to get historical prices for " + exchangeId + "/" + ticker + "-" + interval + " from iexTrading.");
            } else {
                logger.info("Got historical prices for " + exchangeId + "/" + ticker + "-" + interval + " from iexTrading.");
            }
        } else {
            logger.info("Got historical prices for " + exchangeId + "/" + ticker + "-" + interval + " from yahoo.");
        }
        logger.debug("Hisotrical Prices = " + prices);
        return prices;
    }

    public List<Quote> getPricesFrom(String exchange, String ticker, LocalDateTime from, INTERVAL interval) {
        List<Quote> quotes = getPrices(exchange, ticker, interval);
        return quotes.parallelStream()
                .filter(quote -> quote.getEndTime() != null && quote.getEndTime().toLocalDateTime().isAfter(from))
                .collect(Collectors.toList());
    }

    public List<Financials> getFinancials(String exchangeId, String ticker) {
        Exchange exchange = exchangeCache.getExchange(exchangeId);
        List<Financials> financials = yahoo.getFinancials(exchange, ticker);

        if (CollectionUtils.isEmpty(financials)) {
            logger.info("Unable to get financials for " + exchangeId + "/" + ticker + " from yahoo.");
            financials = iexTrading.getFinancials(ticker);
            if(CollectionUtils.isEmpty(financials)) {
                logger.info("Unable to get financials for " + exchangeId + "/" + ticker + " from iex trading.");
            } else {
                logger.info("Got financials for " + exchangeId + "/" + ticker + " from iex Trading.");
            }
        } else {
            logger.info("Got financials for " + exchangeId + "/" + ticker + " from yahoo.");
        }

        return financials;
    }

    public Stats getStats(String exchangeId, String ticker) {
        Exchange exchange = exchangeCache.getExchange(exchangeId);
        Stats stats = yahoo.getStats(exchange, ticker);

        if (stats == null || stats.getSharesOutstanding() == null) {
            logger.info("Unable to get Financial stats for " + exchangeId + "/" + ticker + " from yahoo.");
            stats = iexTrading.getStats(ticker);
            if (stats == null || stats.getSharesOutstanding() == null) {
                logger.info("Unable to get Financial Stats for " + exchangeId + "/" + ticker + " from iexTranding.");
                stats = robinhood.getStats(ticker);
                if(stats == null || stats.getSharesOutstanding() == null) {
                    logger.info("Unable to get Financial Stats for " + exchangeId + "/" + ticker + " from robinhood. Stats Query Failed.");
                } else {
                    logger.info("Got Financial Stats for for " + exchangeId + "/" + ticker + " from robinhood");
                }
            } else {
                logger.info("Got Financial Stats for for " + exchangeId + "/" + ticker + " from iex trading");
            }
        } else {
            logger.info("Got Financial Stats for for " + exchangeId + "/" + ticker + " from yahoo");
        }

        logger.debug("Financial Stats = " + stats);
        return stats;
    }

    public List<Fundamentals> searchTickers(String searchstr) {
        return yahoo.searchTickers(searchstr);
    }

    static final Logger logger = LoggerFactory.getLogger(ServiceProvider.class);

}