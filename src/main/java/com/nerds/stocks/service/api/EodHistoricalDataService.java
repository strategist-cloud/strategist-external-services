package com.nerds.stocks.service.api;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.eod_historical_data.EodStock;

@Component
@ConfigurationProperties(prefix = "stocks.keys")
public class EodHistoricalDataService {

    String eodHistoricalApiKey;
    String url = "https://eodhistoricaldata.com/api/fundamentals/";

    public void setEodHistoricalApiKey(String eodHistoricalApiKey) {
        this.eodHistoricalApiKey = eodHistoricalApiKey;
    }

    public EodStock getDetails(String ticker, String exchange) {
        url = url + ticker + "." + exchange + "?api_token=" + eodHistoricalApiKey;

        RestTemplate restTemplate = new RestTemplate();
        EodStock result = restTemplate.getForObject(url, EodStock.class);
        return result;
    }

    public static void main(String [] args) {
        EodHistoricalDataService service = new EodHistoricalDataService();
        service.setEodHistoricalApiKey("5c2820319734a4.18087221");
        EodStock result = service.getDetails("AAPL", "US");

        System.out.println(result);

    }
}