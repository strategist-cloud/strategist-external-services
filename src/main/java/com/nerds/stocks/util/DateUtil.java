package com.nerds.stocks.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.nerds.stocks.service.data.INTERVAL;

public class DateUtil {

    public static LocalDateTime getLocalDateTime(Calendar calendar) {
        if (calendar == null) {
            return null;
        }

        TimeZone timezone = calendar.getTimeZone();
        ZoneId zone = timezone == null ? ZoneId.systemDefault() : timezone.toZoneId();

        return LocalDateTime.ofInstant(calendar.toInstant(), zone);
    }

    public static Timestamp getTimestamp(LocalDateTime localDateTime) {
        if (localDateTime != null) {
            return new Timestamp(localDateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
        }
        return null;
    }

    public static Timestamp getStartTime(LocalDateTime closedLocalDateTime, INTERVAL interval) {
        if (closedLocalDateTime != null) {
            LocalDateTime closedLocalDateTimeCopy = LocalDateTime.of(closedLocalDateTime.toLocalDate(),
                    closedLocalDateTime.toLocalTime());
            if (interval == INTERVAL._5MIN) {
                closedLocalDateTimeCopy = closedLocalDateTimeCopy.minusMinutes(5);
            } else if (interval == INTERVAL._30MIN) {
                closedLocalDateTimeCopy = closedLocalDateTimeCopy.minusMinutes(30);
            } else if (interval == INTERVAL.DAILY) {
                closedLocalDateTimeCopy = closedLocalDateTimeCopy.minusDays(1);
            } else if (interval == INTERVAL.WEEKLY) {
                closedLocalDateTimeCopy = closedLocalDateTimeCopy.minusWeeks(5);
            }
            return getTimestamp(closedLocalDateTime);
        }
        return null;
    }

    public static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

}