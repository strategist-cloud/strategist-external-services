
package com.nerds.stocks.data.yahoo.search.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "explains",
    "count",
    "quotes",
    "news",
    "nav",
    "lists",
    "totalTime",
    "timeTakenForQuotes",
    "timeTakenForNews",
    "timeTakenForAlgowatchlist",
    "timeTakenForPredefinedScreener",
    "timeTakenForCrunchbase",
    "timeTakenForNav"
})
public class YahooSearchResponse {

    @JsonProperty("explains")
    private java.util.List<Object> explains = null;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("quotes")
    private java.util.List<Quote> quotes = null;
    @JsonProperty("news")
    private java.util.List<News> news = null;
    @JsonProperty("nav")
    private java.util.List<Object> nav = null;
    @JsonProperty("lists")
    private java.util.List<List> lists = null;
    @JsonProperty("totalTime")
    private Integer totalTime;
    @JsonProperty("timeTakenForQuotes")
    private Integer timeTakenForQuotes;
    @JsonProperty("timeTakenForNews")
    private Integer timeTakenForNews;
    @JsonProperty("timeTakenForAlgowatchlist")
    private Integer timeTakenForAlgowatchlist;
    @JsonProperty("timeTakenForPredefinedScreener")
    private Integer timeTakenForPredefinedScreener;
    @JsonProperty("timeTakenForCrunchbase")
    private Integer timeTakenForCrunchbase;
    @JsonProperty("timeTakenForNav")
    private Integer timeTakenForNav;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("explains")
    public java.util.List<Object> getExplains() {
        return explains;
    }

    @JsonProperty("explains")
    public void setExplains(java.util.List<Object> explains) {
        this.explains = explains;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("quotes")
    public java.util.List<Quote> getQuotes() {
        return quotes;
    }

    @JsonProperty("quotes")
    public void setQuotes(java.util.List<Quote> quotes) {
        this.quotes = quotes;
    }

    @JsonProperty("news")
    public java.util.List<News> getNews() {
        return news;
    }

    @JsonProperty("news")
    public void setNews(java.util.List<News> news) {
        this.news = news;
    }

    @JsonProperty("nav")
    public java.util.List<Object> getNav() {
        return nav;
    }

    @JsonProperty("nav")
    public void setNav(java.util.List<Object> nav) {
        this.nav = nav;
    }

    @JsonProperty("lists")
    public java.util.List<List> getLists() {
        return lists;
    }

    @JsonProperty("lists")
    public void setLists(java.util.List<List> lists) {
        this.lists = lists;
    }

    @JsonProperty("totalTime")
    public Integer getTotalTime() {
        return totalTime;
    }

    @JsonProperty("totalTime")
    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    @JsonProperty("timeTakenForQuotes")
    public Integer getTimeTakenForQuotes() {
        return timeTakenForQuotes;
    }

    @JsonProperty("timeTakenForQuotes")
    public void setTimeTakenForQuotes(Integer timeTakenForQuotes) {
        this.timeTakenForQuotes = timeTakenForQuotes;
    }

    @JsonProperty("timeTakenForNews")
    public Integer getTimeTakenForNews() {
        return timeTakenForNews;
    }

    @JsonProperty("timeTakenForNews")
    public void setTimeTakenForNews(Integer timeTakenForNews) {
        this.timeTakenForNews = timeTakenForNews;
    }

    @JsonProperty("timeTakenForAlgowatchlist")
    public Integer getTimeTakenForAlgowatchlist() {
        return timeTakenForAlgowatchlist;
    }

    @JsonProperty("timeTakenForAlgowatchlist")
    public void setTimeTakenForAlgowatchlist(Integer timeTakenForAlgowatchlist) {
        this.timeTakenForAlgowatchlist = timeTakenForAlgowatchlist;
    }

    @JsonProperty("timeTakenForPredefinedScreener")
    public Integer getTimeTakenForPredefinedScreener() {
        return timeTakenForPredefinedScreener;
    }

    @JsonProperty("timeTakenForPredefinedScreener")
    public void setTimeTakenForPredefinedScreener(Integer timeTakenForPredefinedScreener) {
        this.timeTakenForPredefinedScreener = timeTakenForPredefinedScreener;
    }

    @JsonProperty("timeTakenForCrunchbase")
    public Integer getTimeTakenForCrunchbase() {
        return timeTakenForCrunchbase;
    }

    @JsonProperty("timeTakenForCrunchbase")
    public void setTimeTakenForCrunchbase(Integer timeTakenForCrunchbase) {
        this.timeTakenForCrunchbase = timeTakenForCrunchbase;
    }

    @JsonProperty("timeTakenForNav")
    public Integer getTimeTakenForNav() {
        return timeTakenForNav;
    }

    @JsonProperty("timeTakenForNav")
    public void setTimeTakenForNav(Integer timeTakenForNav) {
        this.timeTakenForNav = timeTakenForNav;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
