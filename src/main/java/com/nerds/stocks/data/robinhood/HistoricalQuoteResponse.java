package com.nerds.stocks.data.robinhood;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "quote", "symbol", "interval", "span", "bounds", "instrument", "historicals" })
public class HistoricalQuoteResponse {

    @JsonProperty("quote")
    private String quote;
    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("interval")
    private String interval;
    @JsonProperty("span")
    private String span;
    @JsonProperty("bounds")
    private String bounds;
    @JsonProperty("instrument")
    private String instrument;
    @JsonProperty("historicals")
    private List<Historical> historicals = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("quote")
    public String getQuote() {
        return quote;
    }

    @JsonProperty("quote")
    public void setQuote(String quote) {
        this.quote = quote;
    }

    public HistoricalQuoteResponse withQuote(String quote) {
        this.quote = quote;
        return this;
    }

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public HistoricalQuoteResponse withSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    @JsonProperty("interval")
    public String getInterval() {
        return interval;
    }

    @JsonProperty("interval")
    public void setInterval(String interval) {
        this.interval = interval;
    }

    public HistoricalQuoteResponse withInterval(String interval) {
        this.interval = interval;
        return this;
    }

    @JsonProperty("span")
    public String getSpan() {
        return span;
    }

    @JsonProperty("span")
    public void setSpan(String span) {
        this.span = span;
    }

    public HistoricalQuoteResponse withSpan(String span) {
        this.span = span;
        return this;
    }

    @JsonProperty("bounds")
    public String getBounds() {
        return bounds;
    }

    @JsonProperty("bounds")
    public void setBounds(String bounds) {
        this.bounds = bounds;
    }

    public HistoricalQuoteResponse withBounds(String bounds) {
        this.bounds = bounds;
        return this;
    }

    @JsonProperty("instrument")
    public String getInstrument() {
        return instrument;
    }

    @JsonProperty("instrument")
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public HistoricalQuoteResponse withInstrument(String instrument) {
        this.instrument = instrument;
        return this;
    }

    @JsonProperty("historicals")
    public List<Historical> getHistoricals() {
        return historicals;
    }

    @JsonProperty("historicals")
    public void setHistoricals(List<Historical> historicals) {
        this.historicals = historicals;
    }

    public HistoricalQuoteResponse withHistoricals(List<Historical> historicals) {
        this.historicals = historicals;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public HistoricalQuoteResponse withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}