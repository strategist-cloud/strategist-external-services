package com.nerds.stocks.data.robinhood;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "open", "high", "low", "volume", "average_volume_2_weeks", "average_volume", "high_52_weeks",
        "dividend_yield", "low_52_weeks", "market_cap", "pe_ratio", "shares_outstanding", "description", "instrument",
        "ceo", "headquarters_city", "headquarters_state", "sector", "industry", "num_employees", "year_founded" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class FundamentalResult {

    @JsonProperty("open")
    private String open;
    @JsonProperty("high")
    private String high;
    @JsonProperty("low")
    private String low;
    @JsonProperty("volume")
    private String volume;
    @JsonProperty("average_volume_2_weeks")
    private String averageVolume2Weeks;
    @JsonProperty("average_volume")
    private String averageVolume;
    @JsonProperty("high_52_weeks")
    private String high52Weeks;
    @JsonProperty("dividend_yield")
    private String dividendYield;
    @JsonProperty("low_52_weeks")
    private String low52Weeks;
    @JsonProperty("market_cap")
    private String marketCap;
    @JsonProperty("pe_ratio")
    private String peRatio;
    @JsonProperty("shares_outstanding")
    private String sharesOutstanding;
    @JsonProperty("description")
    private String description;
    @JsonProperty("instrument")
    private String instrument;
    @JsonProperty("ceo")
    private String ceo;
    @JsonProperty("headquarters_city")
    private String headquartersCity;
    @JsonProperty("headquarters_state")
    private String headquartersState;
    @JsonProperty("sector")
    private String sector;
    @JsonProperty("industry")
    private String industry;
    @JsonProperty("num_employees")
    private Integer numEmployees;
    @JsonProperty("year_founded")
    private Integer yearFounded;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("open")
    public String getOpen() {
        return open;
    }

    @JsonProperty("open")
    public void setOpen(String open) {
        this.open = open;
    }

    public FundamentalResult withOpen(String open) {
        this.open = open;
        return this;
    }

    @JsonProperty("high")
    public String getHigh() {
        return high;
    }

    @JsonProperty("high")
    public void setHigh(String high) {
        this.high = high;
    }

    public FundamentalResult withHigh(String high) {
        this.high = high;
        return this;
    }

    @JsonProperty("low")
    public String getLow() {
        return low;
    }

    @JsonProperty("low")
    public void setLow(String low) {
        this.low = low;
    }

    public FundamentalResult withLow(String low) {
        this.low = low;
        return this;
    }

    @JsonProperty("volume")
    public String getVolume() {
        return volume;
    }

    @JsonProperty("volume")
    public void setVolume(String volume) {
        this.volume = volume;
    }

    public FundamentalResult withVolume(String volume) {
        this.volume = volume;
        return this;
    }

    @JsonProperty("average_volume_2_weeks")
    public String getAverageVolume2Weeks() {
        return averageVolume2Weeks;
    }

    @JsonProperty("average_volume_2_weeks")
    public void setAverageVolume2Weeks(String averageVolume2Weeks) {
        this.averageVolume2Weeks = averageVolume2Weeks;
    }

    public FundamentalResult withAverageVolume2Weeks(String averageVolume2Weeks) {
        this.averageVolume2Weeks = averageVolume2Weeks;
        return this;
    }

    @JsonProperty("average_volume")
    public String getAverageVolume() {
        return averageVolume;
    }

    @JsonProperty("average_volume")
    public void setAverageVolume(String averageVolume) {
        this.averageVolume = averageVolume;
    }

    public FundamentalResult withAverageVolume(String averageVolume) {
        this.averageVolume = averageVolume;
        return this;
    }

    @JsonProperty("high_52_weeks")
    public String getHigh52Weeks() {
        return high52Weeks;
    }

    @JsonProperty("high_52_weeks")
    public void setHigh52Weeks(String high52Weeks) {
        this.high52Weeks = high52Weeks;
    }

    public FundamentalResult withHigh52Weeks(String high52Weeks) {
        this.high52Weeks = high52Weeks;
        return this;
    }

    @JsonProperty("dividend_yield")
    public String getDividendYield() {
        return dividendYield;
    }

    @JsonProperty("dividend_yield")
    public void setDividendYield(String dividendYield) {
        this.dividendYield = dividendYield;
    }

    public FundamentalResult withDividendYield(String dividendYield) {
        this.dividendYield = dividendYield;
        return this;
    }

    @JsonProperty("low_52_weeks")
    public String getLow52Weeks() {
        return low52Weeks;
    }

    @JsonProperty("low_52_weeks")
    public void setLow52Weeks(String low52Weeks) {
        this.low52Weeks = low52Weeks;
    }

    public FundamentalResult withLow52Weeks(String low52Weeks) {
        this.low52Weeks = low52Weeks;
        return this;
    }

    @JsonProperty("market_cap")
    public String getMarketCap() {
        return marketCap;
    }

    @JsonProperty("market_cap")
    public void setMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    public FundamentalResult withMarketCap(String marketCap) {
        this.marketCap = marketCap;
        return this;
    }

    @JsonProperty("pe_ratio")
    public String getPeRatio() {
        return peRatio;
    }

    @JsonProperty("pe_ratio")
    public void setPeRatio(String peRatio) {
        this.peRatio = peRatio;
    }

    public FundamentalResult withPeRatio(String peRatio) {
        this.peRatio = peRatio;
        return this;
    }

    @JsonProperty("shares_outstanding")
    public String getSharesOutstanding() {
        return sharesOutstanding;
    }

    @JsonProperty("shares_outstanding")
    public void setSharesOutstanding(String sharesOutstanding) {
        this.sharesOutstanding = sharesOutstanding;
    }

    public FundamentalResult withSharesOutstanding(String sharesOutstanding) {
        this.sharesOutstanding = sharesOutstanding;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public FundamentalResult withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("instrument")
    public String getInstrument() {
        return instrument;
    }

    @JsonProperty("instrument")
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public FundamentalResult withInstrument(String instrument) {
        this.instrument = instrument;
        return this;
    }

    @JsonProperty("ceo")
    public String getCeo() {
        return ceo;
    }

    @JsonProperty("ceo")
    public void setCeo(String ceo) {
        this.ceo = ceo;
    }

    public FundamentalResult withCeo(String ceo) {
        this.ceo = ceo;
        return this;
    }

    @JsonProperty("headquarters_city")
    public String getHeadquartersCity() {
        return headquartersCity;
    }

    @JsonProperty("headquarters_city")
    public void setHeadquartersCity(String headquartersCity) {
        this.headquartersCity = headquartersCity;
    }

    public FundamentalResult withHeadquartersCity(String headquartersCity) {
        this.headquartersCity = headquartersCity;
        return this;
    }

    @JsonProperty("headquarters_state")
    public String getHeadquartersState() {
        return headquartersState;
    }

    @JsonProperty("headquarters_state")
    public void setHeadquartersState(String headquartersState) {
        this.headquartersState = headquartersState;
    }

    public FundamentalResult withHeadquartersState(String headquartersState) {
        this.headquartersState = headquartersState;
        return this;
    }

    @JsonProperty("sector")
    public String getSector() {
        return sector;
    }

    @JsonProperty("sector")
    public void setSector(String sector) {
        this.sector = sector;
    }

    public FundamentalResult withSector(String sector) {
        this.sector = sector;
        return this;
    }

    @JsonProperty("industry")
    public String getIndustry() {
        return industry;
    }

    @JsonProperty("industry")
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public FundamentalResult withIndustry(String industry) {
        this.industry = industry;
        return this;
    }

    @JsonProperty("num_employees")
    public Integer getNumEmployees() {
        return numEmployees;
    }

    @JsonProperty("num_employees")
    public void setNumEmployees(Integer numEmployees) {
        this.numEmployees = numEmployees;
    }

    public FundamentalResult withNumEmployees(Integer numEmployees) {
        this.numEmployees = numEmployees;
        return this;
    }

    @JsonProperty("year_founded")
    public Integer getYearFounded() {
        return yearFounded;
    }

    @JsonProperty("year_founded")
    public void setYearFounded(Integer yearFounded) {
        this.yearFounded = yearFounded;
    }

    public FundamentalResult withYearFounded(Integer yearFounded) {
        this.yearFounded = yearFounded;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public FundamentalResult withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}