package com.nerds.stocks.data.robinhood;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ask_price", "ask_size", "bid_price", "bid_size", "last_trade_price",
        "last_extended_hours_trade_price", "previous_close", "adjusted_previous_close", "previous_close_date", "symbol",
        "trading_halted", "has_traded", "last_trade_price_source", "updated_at", "instrument" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteResult {

    @JsonProperty("ask_price")
    private String askPrice;
    @JsonProperty("ask_size")
    private Integer askSize;
    @JsonProperty("bid_price")
    private String bidPrice;
    @JsonProperty("bid_size")
    private Integer bidSize;
    @JsonProperty("last_trade_price")
    private String lastTradePrice;
    @JsonProperty("last_extended_hours_trade_price")
    private String lastExtendedHoursTradePrice;
    @JsonProperty("previous_close")
    private String previousClose;
    @JsonProperty("adjusted_previous_close")
    private String adjustedPreviousClose;
    @JsonProperty("previous_close_date")
    private String previousCloseDate;
    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("trading_halted")
    private Boolean tradingHalted;
    @JsonProperty("has_traded")
    private Boolean hasTraded;
    @JsonProperty("last_trade_price_source")
    private String lastTradePriceSource;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("instrument")
    private String instrument;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ask_price")
    public String getAskPrice() {
        return askPrice;
    }

    @JsonProperty("ask_price")
    public void setAskPrice(String askPrice) {
        this.askPrice = askPrice;
    }

    public QuoteResult withAskPrice(String askPrice) {
        this.askPrice = askPrice;
        return this;
    }

    @JsonProperty("ask_size")
    public Integer getAskSize() {
        return askSize;
    }

    @JsonProperty("ask_size")
    public void setAskSize(Integer askSize) {
        this.askSize = askSize;
    }

    public QuoteResult withAskSize(Integer askSize) {
        this.askSize = askSize;
        return this;
    }

    @JsonProperty("bid_price")
    public String getBidPrice() {
        return bidPrice;
    }

    @JsonProperty("bid_price")
    public void setBidPrice(String bidPrice) {
        this.bidPrice = bidPrice;
    }

    public QuoteResult withBidPrice(String bidPrice) {
        this.bidPrice = bidPrice;
        return this;
    }

    @JsonProperty("bid_size")
    public Integer getBidSize() {
        return bidSize;
    }

    @JsonProperty("bid_size")
    public void setBidSize(Integer bidSize) {
        this.bidSize = bidSize;
    }

    public QuoteResult withBidSize(Integer bidSize) {
        this.bidSize = bidSize;
        return this;
    }

    @JsonProperty("last_trade_price")
    public String getLastTradePrice() {
        return lastTradePrice;
    }

    @JsonProperty("last_trade_price")
    public void setLastTradePrice(String lastTradePrice) {
        this.lastTradePrice = lastTradePrice;
    }

    public QuoteResult withLastTradePrice(String lastTradePrice) {
        this.lastTradePrice = lastTradePrice;
        return this;
    }

    @JsonProperty("last_extended_hours_trade_price")
    public String getLastExtendedHoursTradePrice() {
        return lastExtendedHoursTradePrice;
    }

    @JsonProperty("last_extended_hours_trade_price")
    public void setLastExtendedHoursTradePrice(String lastExtendedHoursTradePrice) {
        this.lastExtendedHoursTradePrice = lastExtendedHoursTradePrice;
    }

    public QuoteResult withLastExtendedHoursTradePrice(String lastExtendedHoursTradePrice) {
        this.lastExtendedHoursTradePrice = lastExtendedHoursTradePrice;
        return this;
    }

    @JsonProperty("previous_close")
    public String getPreviousClose() {
        return previousClose;
    }

    @JsonProperty("previous_close")
    public void setPreviousClose(String previousClose) {
        this.previousClose = previousClose;
    }

    public QuoteResult withPreviousClose(String previousClose) {
        this.previousClose = previousClose;
        return this;
    }

    @JsonProperty("adjusted_previous_close")
    public String getAdjustedPreviousClose() {
        return adjustedPreviousClose;
    }

    @JsonProperty("adjusted_previous_close")
    public void setAdjustedPreviousClose(String adjustedPreviousClose) {
        this.adjustedPreviousClose = adjustedPreviousClose;
    }

    public QuoteResult withAdjustedPreviousClose(String adjustedPreviousClose) {
        this.adjustedPreviousClose = adjustedPreviousClose;
        return this;
    }

    @JsonProperty("previous_close_date")
    public String getPreviousCloseDate() {
        return previousCloseDate;
    }

    @JsonProperty("previous_close_date")
    public void setPreviousCloseDate(String previousCloseDate) {
        this.previousCloseDate = previousCloseDate;
    }

    public QuoteResult withPreviousCloseDate(String previousCloseDate) {
        this.previousCloseDate = previousCloseDate;
        return this;
    }

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public QuoteResult withSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    @JsonProperty("trading_halted")
    public Boolean getTradingHalted() {
        return tradingHalted;
    }

    @JsonProperty("trading_halted")
    public void setTradingHalted(Boolean tradingHalted) {
        this.tradingHalted = tradingHalted;
    }

    public QuoteResult withTradingHalted(Boolean tradingHalted) {
        this.tradingHalted = tradingHalted;
        return this;
    }

    @JsonProperty("has_traded")
    public Boolean getHasTraded() {
        return hasTraded;
    }

    @JsonProperty("has_traded")
    public void setHasTraded(Boolean hasTraded) {
        this.hasTraded = hasTraded;
    }

    public QuoteResult withHasTraded(Boolean hasTraded) {
        this.hasTraded = hasTraded;
        return this;
    }

    @JsonProperty("last_trade_price_source")
    public String getLastTradePriceSource() {
        return lastTradePriceSource;
    }

    @JsonProperty("last_trade_price_source")
    public void setLastTradePriceSource(String lastTradePriceSource) {
        this.lastTradePriceSource = lastTradePriceSource;
    }

    public QuoteResult withLastTradePriceSource(String lastTradePriceSource) {
        this.lastTradePriceSource = lastTradePriceSource;
        return this;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public QuoteResult withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @JsonProperty("instrument")
    public String getInstrument() {
        return instrument;
    }

    @JsonProperty("instrument")
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public QuoteResult withInstrument(String instrument) {
        this.instrument = instrument;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public QuoteResult withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}