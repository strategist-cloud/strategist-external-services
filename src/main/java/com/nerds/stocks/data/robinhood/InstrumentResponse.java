package com.nerds.stocks.data.robinhood;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "margin_initial_ratio", "rhs_tradability", "id", "market", "simple_name", "min_tick_size",
        "maintenance_ratio", "tradability", "state", "type", "tradeable", "fundamentals", "quote", "symbol",
        "day_trade_ratio", "name", "tradable_chain_id", "splits", "url", "country", "bloomberg_unique", "list_date" })
public class InstrumentResponse {

    @JsonProperty("margin_initial_ratio")
    private String marginInitialRatio;
    @JsonProperty("rhs_tradability")
    private String rhsTradability;
    @JsonProperty("id")
    private String id;
    @JsonProperty("market")
    private String market;
    @JsonProperty("simple_name")
    private String simpleName;
    @JsonProperty("min_tick_size")
    private Object minTickSize;
    @JsonProperty("maintenance_ratio")
    private String maintenanceRatio;
    @JsonProperty("tradability")
    private String tradability;
    @JsonProperty("state")
    private String state;
    @JsonProperty("type")
    private String type;
    @JsonProperty("tradeable")
    private Boolean tradeable;
    @JsonProperty("fundamentals")
    private String fundamentals;
    @JsonProperty("quote")
    private String quote;
    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("day_trade_ratio")
    private String dayTradeRatio;
    @JsonProperty("name")
    private String name;
    @JsonProperty("tradable_chain_id")
    private String tradableChainId;
    @JsonProperty("splits")
    private String splits;
    @JsonProperty("url")
    private String url;
    @JsonProperty("country")
    private String country;
    @JsonProperty("bloomberg_unique")
    private String bloombergUnique;
    @JsonProperty("list_date")
    private String listDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("margin_initial_ratio")
    public String getMarginInitialRatio() {
        return marginInitialRatio;
    }

    @JsonProperty("margin_initial_ratio")
    public void setMarginInitialRatio(String marginInitialRatio) {
        this.marginInitialRatio = marginInitialRatio;
    }

    public InstrumentResponse withMarginInitialRatio(String marginInitialRatio) {
        this.marginInitialRatio = marginInitialRatio;
        return this;
    }

    @JsonProperty("rhs_tradability")
    public String getRhsTradability() {
        return rhsTradability;
    }

    @JsonProperty("rhs_tradability")
    public void setRhsTradability(String rhsTradability) {
        this.rhsTradability = rhsTradability;
    }

    public InstrumentResponse withRhsTradability(String rhsTradability) {
        this.rhsTradability = rhsTradability;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public InstrumentResponse withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("market")
    public String getMarket() {
        return market;
    }

    @JsonProperty("market")
    public void setMarket(String market) {
        this.market = market;
    }

    public InstrumentResponse withMarket(String market) {
        this.market = market;
        return this;
    }

    @JsonProperty("simple_name")
    public String getSimpleName() {
        return simpleName;
    }

    @JsonProperty("simple_name")
    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public InstrumentResponse withSimpleName(String simpleName) {
        this.simpleName = simpleName;
        return this;
    }

    @JsonProperty("min_tick_size")
    public Object getMinTickSize() {
        return minTickSize;
    }

    @JsonProperty("min_tick_size")
    public void setMinTickSize(Object minTickSize) {
        this.minTickSize = minTickSize;
    }

    public InstrumentResponse withMinTickSize(Object minTickSize) {
        this.minTickSize = minTickSize;
        return this;
    }

    @JsonProperty("maintenance_ratio")
    public String getMaintenanceRatio() {
        return maintenanceRatio;
    }

    @JsonProperty("maintenance_ratio")
    public void setMaintenanceRatio(String maintenanceRatio) {
        this.maintenanceRatio = maintenanceRatio;
    }

    public InstrumentResponse withMaintenanceRatio(String maintenanceRatio) {
        this.maintenanceRatio = maintenanceRatio;
        return this;
    }

    @JsonProperty("tradability")
    public String getTradability() {
        return tradability;
    }

    @JsonProperty("tradability")
    public void setTradability(String tradability) {
        this.tradability = tradability;
    }

    public InstrumentResponse withTradability(String tradability) {
        this.tradability = tradability;
        return this;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    public InstrumentResponse withState(String state) {
        this.state = state;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public InstrumentResponse withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("tradeable")
    public Boolean getTradeable() {
        return tradeable;
    }

    @JsonProperty("tradeable")
    public void setTradeable(Boolean tradeable) {
        this.tradeable = tradeable;
    }

    public InstrumentResponse withTradeable(Boolean tradeable) {
        this.tradeable = tradeable;
        return this;
    }

    @JsonProperty("fundamentals")
    public String getFundamentals() {
        return fundamentals;
    }

    @JsonProperty("fundamentals")
    public void setFundamentals(String fundamentals) {
        this.fundamentals = fundamentals;
    }

    public InstrumentResponse withFundamentals(String fundamentals) {
        this.fundamentals = fundamentals;
        return this;
    }

    @JsonProperty("quote")
    public String getQuote() {
        return quote;
    }

    @JsonProperty("quote")
    public void setQuote(String quote) {
        this.quote = quote;
    }

    public InstrumentResponse withQuote(String quote) {
        this.quote = quote;
        return this;
    }

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public InstrumentResponse withSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    @JsonProperty("day_trade_ratio")
    public String getDayTradeRatio() {
        return dayTradeRatio;
    }

    @JsonProperty("day_trade_ratio")
    public void setDayTradeRatio(String dayTradeRatio) {
        this.dayTradeRatio = dayTradeRatio;
    }

    public InstrumentResponse withDayTradeRatio(String dayTradeRatio) {
        this.dayTradeRatio = dayTradeRatio;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public InstrumentResponse withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("tradable_chain_id")
    public String getTradableChainId() {
        return tradableChainId;
    }

    @JsonProperty("tradable_chain_id")
    public void setTradableChainId(String tradableChainId) {
        this.tradableChainId = tradableChainId;
    }

    public InstrumentResponse withTradableChainId(String tradableChainId) {
        this.tradableChainId = tradableChainId;
        return this;
    }

    @JsonProperty("splits")
    public String getSplits() {
        return splits;
    }

    @JsonProperty("splits")
    public void setSplits(String splits) {
        this.splits = splits;
    }

    public InstrumentResponse withSplits(String splits) {
        this.splits = splits;
        return this;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    public InstrumentResponse withUrl(String url) {
        this.url = url;
        return this;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    public InstrumentResponse withCountry(String country) {
        this.country = country;
        return this;
    }

    @JsonProperty("bloomberg_unique")
    public String getBloombergUnique() {
        return bloombergUnique;
    }

    @JsonProperty("bloomberg_unique")
    public void setBloombergUnique(String bloombergUnique) {
        this.bloombergUnique = bloombergUnique;
    }

    public InstrumentResponse withBloombergUnique(String bloombergUnique) {
        this.bloombergUnique = bloombergUnique;
        return this;
    }

    @JsonProperty("list_date")
    public String getListDate() {
        return listDate;
    }

    @JsonProperty("list_date")
    public void setListDate(String listDate) {
        this.listDate = listDate;
    }

    public InstrumentResponse withListDate(String listDate) {
        this.listDate = listDate;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public InstrumentResponse withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}