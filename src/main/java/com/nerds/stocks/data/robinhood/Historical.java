package com.nerds.stocks.data.robinhood;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "begins_at", "open_price", "close_price", "high_price", "low_price", "volume", "session",
        "interpolated" })
public class Historical {

    @JsonProperty("begins_at")
    private String beginsAt;
    @JsonProperty("open_price")
    private String openPrice;
    @JsonProperty("close_price")
    private String closePrice;
    @JsonProperty("high_price")
    private String highPrice;
    @JsonProperty("low_price")
    private String lowPrice;
    @JsonProperty("volume")
    private Integer volume;
    @JsonProperty("session")
    private String session;
    @JsonProperty("interpolated")
    private Boolean interpolated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("begins_at")
    public String getBeginsAt() {
        return beginsAt;
    }

    @JsonProperty("begins_at")
    public void setBeginsAt(String beginsAt) {
        this.beginsAt = beginsAt;
    }

    public Historical withBeginsAt(String beginsAt) {
        this.beginsAt = beginsAt;
        return this;
    }

    @JsonProperty("open_price")
    public String getOpenPrice() {
        return openPrice;
    }

    @JsonProperty("open_price")
    public void setOpenPrice(String openPrice) {
        this.openPrice = openPrice;
    }

    public Historical withOpenPrice(String openPrice) {
        this.openPrice = openPrice;
        return this;
    }

    @JsonProperty("close_price")
    public String getClosePrice() {
        return closePrice;
    }

    @JsonProperty("close_price")
    public void setClosePrice(String closePrice) {
        this.closePrice = closePrice;
    }

    public Historical withClosePrice(String closePrice) {
        this.closePrice = closePrice;
        return this;
    }

    @JsonProperty("high_price")
    public String getHighPrice() {
        return highPrice;
    }

    @JsonProperty("high_price")
    public void setHighPrice(String highPrice) {
        this.highPrice = highPrice;
    }

    public Historical withHighPrice(String highPrice) {
        this.highPrice = highPrice;
        return this;
    }

    @JsonProperty("low_price")
    public String getLowPrice() {
        return lowPrice;
    }

    @JsonProperty("low_price")
    public void setLowPrice(String lowPrice) {
        this.lowPrice = lowPrice;
    }

    public Historical withLowPrice(String lowPrice) {
        this.lowPrice = lowPrice;
        return this;
    }

    @JsonProperty("volume")
    public Integer getVolume() {
        return volume;
    }

    @JsonProperty("volume")
    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Historical withVolume(Integer volume) {
        this.volume = volume;
        return this;
    }

    @JsonProperty("session")
    public String getSession() {
        return session;
    }

    @JsonProperty("session")
    public void setSession(String session) {
        this.session = session;
    }

    public Historical withSession(String session) {
        this.session = session;
        return this;
    }

    @JsonProperty("interpolated")
    public Boolean getInterpolated() {
        return interpolated;
    }

    @JsonProperty("interpolated")
    public void setInterpolated(Boolean interpolated) {
        this.interpolated = interpolated;
    }

    public Historical withInterpolated(Boolean interpolated) {
        this.interpolated = interpolated;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Historical withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}