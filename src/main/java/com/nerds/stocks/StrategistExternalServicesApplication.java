package com.nerds.stocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@EnableConfigurationProperties
public class StrategistExternalServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrategistExternalServicesApplication.class, args);
	}

}
