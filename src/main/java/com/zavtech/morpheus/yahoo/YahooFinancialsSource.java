/**
 * Copyright (C) 2014-2017 Xavier Witdouck
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zavtech.morpheus.yahoo;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.zavtech.morpheus.array.Array;
import com.zavtech.morpheus.frame.DataFrame;
import com.zavtech.morpheus.frame.DataFrameException;
import com.zavtech.morpheus.frame.DataFrameSource;
import com.zavtech.morpheus.util.http.HttpClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

/**
 * Any use of the extracted data from this software should adhere to Yahoo
 * Finance Terms and Conditions.
 *
 * @author Xavier Witdouck
 *
 *         <p>
 *         <strong>This is open source software released under the
 *         <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache 2.0
 *         License</a></strong>
 *         </p>
 */
@Component
public class YahooFinancialsSource extends DataFrameSource<LocalDate, YahooFieldExt, YahooFinancialsSource.Options> {

    private static final Array<YahooFieldExt> fields = Array.of(
        YahooFieldExt.EARNINGS_TOTAL_REVENUE,
        YahooFieldExt.COST_OF_REVENUE,
        YahooFieldExt.EARNINGS_GROSS_PROFIT,
        YahooFieldExt.OPERATING_REVENUE,
        YahooFieldExt.OPERATING_INCOME,
        YahooFieldExt.NET_INCOME,
        YahooFieldExt.RESEARCH_AND_DEVELOPMENT,
        YahooFieldExt.OPERATING_EXPENSE,
        YahooFieldExt.CURRENT_ASSETS,
        YahooFieldExt.TOTAL_ASSETS,
        YahooFieldExt.TOTAL_LIABILITIES,
        YahooFieldExt.CURRENT_CASH,
        YahooFieldExt.CURRENT_DEBT,
        YahooFieldExt.TOTAL_CASH,
        YahooFieldExt.TOTAL_DEBT,
        YahooFieldExt.SHARE_HOLDER_EQUITY,
        YahooFieldExt.CASH_CHANGE,
        YahooFieldExt.CASH_FLOW,
        YahooFieldExt.OPERATING_GAINS_LOSSES,
        YahooFieldExt.REPORT_DATE,
        YahooFieldExt.PREFERRED_SHARES,
        YahooFieldExt.COMMON_SHARES,
        YahooFieldExt.TREASURY_SHARES,
        YahooFieldExt.OUTSTANDING_SHARES
    );

    private String baseUrl;
    private DateTimeFormatter endingDateFormmatter = DateTimeFormatter.ofPattern("M/d/yyyy");

    /**
     * Constructor
     */
    public YahooFinancialsSource() {
        this("https://finance.yahoo.com/quote/%s/financials?p=%s");
    }

    /**
     * Constructor
     * @param urlTemplate   the URL template on which to replace the security ticker
     */
    public YahooFinancialsSource(String urlTemplate) {
        this.baseUrl = urlTemplate.substring(0, urlTemplate.indexOf('/', 9));
    }

    /**
     * Returns the fields supported by this source
     * @return  the fields supported by this source
     */
    Array<YahooFieldExt> getFields() {
        return fields;
    }

    @Override
    public DataFrame<LocalDate, YahooFieldExt> read(Consumer<Options> configurator) throws DataFrameException {
        final Options options = initOptions(new Options(), configurator);
        final String ticker = options.ticker;
        try {
            final DataFrame<LocalDate, YahooFieldExt> frame = createFrame(new HashSet<LocalDate>());
            this.captureIncome(ticker, frame);
            this.captureBalanceSheet(ticker, frame);
            this.captureCashFlow(ticker, frame);
            
            
            return frame;
        } catch (Exception ex) {
            throw new DataFrameException("Failed to query statistics for one or more tickers: " + ticker, ex);
        }
    }

    /**
     * Returns the DataFrame to capture results for request
     * @param tickers   the tickers
     * @return          the DataFrame result
     */
    private DataFrame<LocalDate,YahooFieldExt> createFrame(Set<LocalDate> endingDates) {
        return DataFrame.of(endingDates, YahooFieldExt.class, columns -> {
            fields.forEach(field -> {
                final Class<?> dataType = field.getDataType();
                final Array<?> array = Array.of(dataType, endingDates.size());
                columns.add(field, array);
            });
        });
    }

    /**
     * Captures Income Info from the document
     * @param endingDate    the asset ticker
     * @param document  the document
     * @param frame    the result matrix
     */
    private void captureIncome(String ticker, DataFrame<LocalDate, YahooFieldExt> frame) {
        try {
            final String url = String.format("https://finance.yahoo.com/quote/%s/financials?p=%s", ticker, ticker);
            HttpClient.getDefault().doGet(httpRequest -> {
                httpRequest.setUrl(url);
                httpRequest.getHeaders().putAll(YahooFinanceExt.getRequestHeaders());
                httpRequest.setResponseHandler(response -> {
                    try {
                        final long t1 = System.currentTimeMillis();
                        final InputStream stream = response.getStream();
                        final Document document = Jsoup.parse(stream, "UTF-8", baseUrl);
                        final long t2 = System.currentTimeMillis();
                        
                        final YahooFinanceParser parser = new YahooFinanceParser();
                        final Map<LocalDate, Map<String, String>> attributeMap = parseAttributes(document, "Revenue");
                        frame.addAll(createFrame(attributeMap.keySet()));
                        System.out.println("Added dates to data frame: " + attributeMap.keySet());            
                        attributeMap.forEach((endingDate, dateToValueMap) -> {
                            try {
                                dateToValueMap.forEach((key, value) -> {
                                    final Object datumValue = parser.parse(value);
                                    if (key.contains("Total Revenue")) frame.data().setValue(endingDate, YahooFieldExt.EARNINGS_TOTAL_REVENUE, datumValue);
                                    else if (key.startsWith("Cost of Revenue")) frame.data().setValue(endingDate, YahooFieldExt.COST_OF_REVENUE, datumValue);
                                    else if (key.startsWith("Gross Profit")) frame.data().setValue(endingDate, YahooFieldExt.EARNINGS_GROSS_PROFIT, datumValue);
                                    else if (key.startsWith("Research Development")) frame.data().setValue(endingDate, YahooFieldExt.RESEARCH_AND_DEVELOPMENT, datumValue);
                                    //else if (key.startsWith("Income Before Tax")) frame.data().setValue(ticker, YahooFieldExt.OPERATING_REVENUE, datumValue);
                                    else if (key.startsWith("Operating Income or Loss")) frame.data().setValue(endingDate, YahooFieldExt.OPERATING_INCOME, datumValue);
                                    else if (key.startsWith("Net Income From Continuing Ops")) frame.data().setValue(endingDate, YahooFieldExt.OPERATING_GAINS_LOSSES, datumValue);
                                    else if (key.startsWith("Net Income Applicable To Common Shares")) frame.data().setValue(endingDate, YahooFieldExt.NET_INCOME, datumValue);
                                    else if (key.startsWith("Total Operating Expenses")) frame.data().setValue(endingDate, YahooFieldExt.OPERATING_EXPENSE, datumValue);
                                });
                                frame.data().setValue(endingDate, YahooFieldExt.REPORT_DATE, endingDate);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                throw new RuntimeException(String.format("Failed to process field %s", endingDate) , ex);
                            }
                        });
                        
                        final long t3 = System.currentTimeMillis();
                        System.out.println("Processed " + url + " in " + (t3 - t1) + " millis (" + (t2 - t1) + "+" + (t3 - t2) + " millis)");
                        return Optional.empty();
                    } catch (IOException ex) {
                        throw new RuntimeException("Failed to load content from Yahoo Finance: " + url, ex);
                    }
                });
            });
            
        } catch (Exception ex) {
            throw new DataFrameException("Failed to extract statistic for " + ticker, ex);
        }
    }

    /**
     * Captures Income Info from the document
     * @param ticker    the asset ticker
     * @param document  the document
     * @param frame    the result matrix
     */
    private void captureBalanceSheet(String ticker, DataFrame<LocalDate, YahooFieldExt> frame) {
        try {
            final String url = String.format("https://finance.yahoo.com/quote/%s/balance-sheet?p=%s", ticker, ticker);
            HttpClient.getDefault().doGet(httpRequest -> {
                httpRequest.setUrl(url);
                httpRequest.getHeaders().putAll(YahooFinanceExt.getRequestHeaders());
                httpRequest.setResponseHandler(response -> {
                    try {
                        final long t1 = System.currentTimeMillis();
                        final InputStream stream = response.getStream();
                        final Document document = Jsoup.parse(stream, "UTF-8", baseUrl);
                        final long t2 = System.currentTimeMillis();
                        
                        final YahooFinanceParser parser = new YahooFinanceParser();
                        final Map<LocalDate, Map<String, String>> attributeMap = parseAttributes(document, "Period Ending");
                        frame.addAll(createFrame(attributeMap.keySet()));
                        System.out.println("Added dates to data frame: " + attributeMap.keySet());    
                        attributeMap.forEach((endingDate, dateToValueMap) -> {
                            try {
                                dateToValueMap.forEach((key, value) -> {
                                    final Object datumValue = parser.parse(value);
                                    if (key.startsWith("Total Current Assets")) frame.data().setValue(endingDate, YahooFieldExt.CURRENT_ASSETS, datumValue);
                                    else if (key.startsWith("Total Assets")) frame.data().setValue(endingDate, YahooFieldExt.TOTAL_ASSETS, datumValue);
                                    else if (key.startsWith("Total Liabilities")) frame.data().setValue(endingDate, YahooFieldExt.TOTAL_LIABILITIES, datumValue);
                                    else if (key.startsWith("Cash And Cash Equivalents")) frame.data().setValue(endingDate, YahooFieldExt.CURRENT_CASH, datumValue);
                                    else if (key.startsWith("Short/Current Long Term Debt")) frame.data().setValue(endingDate, YahooFieldExt.CURRENT_DEBT, datumValue);
                                    else if (key.startsWith("Total Stockholder Equity")) frame.data().setValue(endingDate, YahooFieldExt.SHARE_HOLDER_EQUITY, datumValue);
                                    else if (key.startsWith("Research Development")) frame.data().setValue(endingDate, YahooFieldExt.REPORT_DATE, datumValue);
                                    else if (key.startsWith("Preferred Stock")) frame.data().setValue(endingDate, YahooFieldExt.PREFERRED_SHARES, datumValue);
                                    else if (key.startsWith("Common Stock")) frame.data().setValue(endingDate, YahooFieldExt.COMMON_SHARES, datumValue);
                                    else if (key.startsWith("Treasury Stock")) frame.data().setValue(endingDate, YahooFieldExt.TREASURY_SHARES, datumValue);
                                });
                                double preferredShares = frame.data().getValue(endingDate, YahooFieldExt.PREFERRED_SHARES), 
                                        commonShares = frame.data().getValue(endingDate, YahooFieldExt.COMMON_SHARES), 
                                        treasuryShares = frame.data().getValue(endingDate, YahooFieldExt.TREASURY_SHARES);
                                frame.data().setValue(endingDate, YahooFieldExt.OUTSTANDING_SHARES, preferredShares + commonShares - treasuryShares);
                                frame.data().setValue(endingDate, YahooFieldExt.REPORT_DATE, endingDate);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                throw new RuntimeException(String.format("Failed to process field %s = ", endingDate) , ex);
                            }
                        });
                        
                        final long t3 = System.currentTimeMillis();
                        System.out.println("Processed " + url + " in " + (t3 - t1) + " millis (" + (t2 - t1) + "+"
                                + (t3 - t2) + " millis)");
                        return Optional.empty();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        throw new RuntimeException("Failed to load content from Yahoo Finance: " + url, ex);
                    }
                });
            });
        } catch (Exception ex) {
            throw new DataFrameException("Failed to extract statistic for " + ticker, ex);
        }
    }

    /**
     * Captures Income Info from the document
     * @param ticker    the asset ticker
     * @param document  the document
     * @param frame    the result matrix
     */
    private void captureCashFlow(String ticker, DataFrame<LocalDate, YahooFieldExt> frame) {
        try {
            final String url = String.format("https://finance.yahoo.com/quote/%s/cash-flow?p=%s", ticker, ticker);
            HttpClient.getDefault().doGet(httpRequest -> {
                httpRequest.setUrl(url);
                httpRequest.getHeaders().putAll(YahooFinanceExt.getRequestHeaders());
                httpRequest.setResponseHandler(response -> {
                    try {
                        final long t1 = System.currentTimeMillis();
                        final InputStream stream = response.getStream();
                        final Document document = Jsoup.parse(stream, "UTF-8", baseUrl);
                        final long t2 = System.currentTimeMillis();

                        final YahooFinanceParser parser = new YahooFinanceParser();
                        final Map<LocalDate, Map<String, String>> attributeMap = parseAttributes(document, "Period Ending");
                        frame.addAll(createFrame(attributeMap.keySet()));
                        System.out.println("Added dates to data frame: " + attributeMap.keySet());    
                        attributeMap.forEach((endingDate, dateToValueMap) -> {
                            try {
                                dateToValueMap.forEach((key, value) -> {
                                    final Object datumValue = parser.parse(value);
                                    if (key.startsWith("Total Cash Flows From Financing Activities")) frame.data().setValue(endingDate, YahooFieldExt.TOTAL_CASH, datumValue);
                                    else if (key.startsWith("Net Borrowings")) frame.data().setValue(endingDate, YahooFieldExt.TOTAL_DEBT, datumValue);
                                    else if (key.startsWith("Total Cash Flows From Financing Activities")) frame.data().setValue(endingDate, YahooFieldExt.CASH_FLOW, datumValue);
                                    else if (key.startsWith("Change In Cash and Cash Equivalents")) frame.data().setValue(endingDate, YahooFieldExt.CASH_CHANGE, datumValue);
                                    else if (key.startsWith("Research Development")) frame.data().setValue(endingDate, YahooFieldExt.REPORT_DATE, datumValue);
                                });
                                frame.data().setValue(endingDate, YahooFieldExt.REPORT_DATE, endingDate);
                                
                            } catch (Exception ex) {
                                throw new RuntimeException(String.format("Failed to process field %s", endingDate) , ex);
                            }

                        });

                        final long t3 = System.currentTimeMillis();
                        System.out.println("Processed " + url + " in " + (t3 - t1) + " millis (" + (t2 - t1) + "+"
                                + (t3 - t2) + " millis)");
                        return Optional.empty();
                    } catch (IOException ex) {
                        throw new RuntimeException("Failed to load content from Yahoo Finance: " + url, ex);
                    }
                });
            });
        } catch (Exception ex) {
            throw new DataFrameException("Failed to extract Cash Flow for " + ticker, ex);
        }
    }


    /**
     * Parses all rows with two cells and returns cell values
     * @param document                  the document to parse
     * @param tableHeadingIdentifier    the string value of first row first column in the table to identify which table to read the data from
     * @return                          the map of property & values
     */
    private Map<LocalDate, Map<String, String>> parseAttributes(Document document, String tableHeadingIdentifier) {
        Map<LocalDate, Map<String, String>> valuesMap = new HashMap<LocalDate, Map<String, String>>();
        final Elements elements = document.select("section[data-test]");
        
        final Elements tables = elements.select("table");
        for(Element table: tables) {
            final Elements rows = table.select("tr");
            if(!rows.isEmpty()) {
                final Elements cols = rows.get(0).select("td");
                
                // Check if the value of first cell in first row(i.e. Column heading) is same as identifier.
                if(!cols.isEmpty() && tableHeadingIdentifier.equalsIgnoreCase(toString(cols.get(0)))) {
                    List<LocalDate> endingDates = cols.subList(1, cols.size())
                                                .parallelStream()
                                                .map(date -> LocalDate.parse(toString(date), endingDateFormmatter))
                                                .collect(Collectors.toList());
                    for (int rowIndex = 1; rowIndex < rows.size(); rowIndex++) {
                        final Element row = rows.get(rowIndex);
                        final Elements cells = row.select("td");
                        final String name = toString(cells.get(0));
                        System.out.print(name);
                        for(int colIndex = 1; colIndex < cells.size(); colIndex++) {
                            System.out.print("\t " + toString(cells.get(colIndex)));
                            Map<String, String> attributeMap = valuesMap.get(endingDates.get(colIndex - 1));
                            if(attributeMap == null) {
                                attributeMap = new HashMap<String, String>();
                                valuesMap.put(endingDates.get(colIndex - 1), attributeMap);
                            }
                            attributeMap.put(name, toString(cells.get(colIndex)));
                        }
                    }
                }
            }
        }
        return valuesMap;
    }


    /**
     * Extracts a terminal value from a table cell element
     * @param cell  the cell element reference
     * @return      the value extracted from cell
     */
    private String toString(Element cell) {
        final Elements span = cell.select("span");
        if (span.size() == 1) {
            final String text = span.get(0).text();
            return text == null ? null : text.trim();
        } else {
            final String text = cell.text();
            return text == null ? null : text.trim();
        }
    }


    /**
     * The options for this source
     */
    public class Options implements DataFrameSource.Options<LocalDate,YahooFieldExt> {

        private String ticker = "";


        @Override
        public void validate() {
            if (ticker == null) throw new IllegalStateException("The ticker cannot be null");
        }

        /**
         * Set the ticker for these options
         * @param ticker   the ticker
         */
        public Options withTicker(String ticker) {
            this.ticker = ticker;
            return this;
        }

    }

    public static void main(String[] args) {
        final String ticker = "AAPL";
        final YahooFinancialsSource source = new YahooFinancialsSource();
        final DataFrame<LocalDate, YahooFieldExt> frame = source.read(o -> o.withTicker(ticker));
        frame.out().print();
    }

}
