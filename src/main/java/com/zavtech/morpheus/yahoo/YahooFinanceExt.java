/**
 * Copyright (C) 2014-2017 Xavier Witdouck
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zavtech.morpheus.yahoo;

import java.net.Inet4Address;
import java.time.LocalDate;
import java.util.Map;

import com.zavtech.morpheus.frame.DataFrame;
import com.zavtech.morpheus.frame.DataFrameSource;
import com.zavtech.morpheus.util.Collect;

import org.springframework.stereotype.Component;

/**
 * A convenience class to expose a more specific API to request data from Yahoo Finance
 *
 * @author Xavier Witdouck
 *
 * <p><strong>This is open source software released under the <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache 2.0 License</a></strong></p>
 */
@Component
public class YahooFinanceExt extends YahooFinance {

    /**
     * Static initializer
     */
    static {
        DataFrameSource.register(new YahooOptionSource());
        DataFrameSource.register(new YahooQuoteHistorySource());
        DataFrameSource.register(new YahooQuoteLiveSource());
        DataFrameSource.register(new YahooReturnSource());
        DataFrameSource.register(new YahooStatsSource());
        DataFrameSource.register(new YahooFinancialsSource());
        DataFrameSource.register(new YahooFundamentalsSourceExt());
    }

    /**
     * Constructor
     */
    public YahooFinanceExt() {
        super();
    }

    public DataFrame<LocalDate, YahooFieldExt> getFinancials(String ticker) {
        return DataFrameSource.lookup(YahooFinancialsSource.class).read(options -> {
            options.withTicker(ticker);
        });
    }

    /**
     * Returns a DataFrame containing equity fundamentals for the set of tickers specified
     * @param tickers   the set of security tickers
     * @return          the DataFrame of tickers
     */
    public DataFrame<String, YahooFieldExt> getFundamentals(String ticker) {
        return DataFrameSource.lookup(YahooFundamentalsSourceExt.class).read(options -> {
            options.withTickers(ticker);
        });
    }

    /**
     * Returns the standard set of headers to make us look like a browser
     * @return      the standard set of request headers
     */
    static Map<String,String> getRequestHeaders() {
        return Collect.asMap(map -> {
            try {
                map.put("User-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
                map.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                map.put("Referer", "https://www.google.com/");
                map.put("Accept-encoding", "gzip, deflate");
                map.put("Accept-language", "en-US,en;q=0.8");
                map.put("Host", Inet4Address.getLocalHost().getHostName());
                map.put("Cache-control", "max-age=259200");
                map.put("Connection", "keep-alive");
            } catch (Exception ex) {
                throw new RuntimeException("Unable to resolve hostnane", ex);
            }
        });
    }

    public static void main(String [] args) {
        
    }

}
