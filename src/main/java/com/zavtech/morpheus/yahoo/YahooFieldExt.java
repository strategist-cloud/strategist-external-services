package com.zavtech.morpheus.yahoo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class YahooFieldExt implements Comparable<YahooFieldExt>, java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private static final Map<Class<?>,Object> nullValueMap = new HashMap<>();
    private static final Map<String,YahooFieldExt> fieldMap = new HashMap<>();

    /**
     * Static initializer
     */
    static {
        nullValueMap.put(Double.class, Double.NaN);
        nullValueMap.put(LocalDate.class, null);
        nullValueMap.put(String.class, null);
    }

    private String name;
    private Object nullValue;
    private Class<?> dataType;


    /**
     * Constructor
     * @param name      the field name
     * @param dataType  the field data type
     * @param nullValue the field null value
     */
    public YahooFieldExt(String name, Class<?> dataType, Object nullValue) {
        if (fieldMap.containsKey(name)) {
            throw new IllegalArgumentException("Field with name already registered: " + name);
        } else {
            this.name = name;
            this.dataType = dataType;
            this.nullValue = nullValue;
        }
    }

    /**
     * Returns the field for the name speciied
     * @param name  the field name
     * @return      the field match
     */
    public static YahooFieldExt getField(String name) {
        return fieldMap.get(name);
    }

    /**
     * Returns the name for this field
     * @return  the field name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the null value for this field
     * @return  the null value
     */
    public Object getNullValue() {
        return nullValue;
    }

    /**
     * Returns the data type for this field
     * @return  the data type for field
     */
    public Class<?> getDataType() {
        return dataType;
    }

    @Override()
    public int hashCode() {
        return name.hashCode();
    }

    @Override()
    public boolean equals(Object other) {
        return other != null && other instanceof YahooField && ((YahooFieldExt)other).name.equals(this.name);
    }

    @Override()
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(YahooFieldExt other) {
        return getName().compareTo(other.getName());
    }

    /**
     * Caches and returns a newly created QuoteField
     * @param fieldName   the field name
     * @param dataType    the field data type
     * @return            the newly created field
     */
    static YahooFieldExt create(String fieldName, Class<?> dataType) {
        try {
            final Object nullValue = nullValueMap.get(dataType);
            final YahooFieldExt field = new YahooFieldExt(fieldName, dataType, nullValue);
            fieldMap.put(fieldName, field);
            return field;
        } catch (Exception ex) {
            throw new RuntimeException("Failed to create quote field for " + fieldName, ex);
        }
    }

    public static final YahooFieldExt TICKER = create("TICKER", String.class);
    public static final YahooFieldExt TIMESTAMP = create("TIMESTAMP",ZonedDateTime.class);
    public static final YahooFieldExt NAME = create("NAME", String.class);
    public static final YahooFieldExt PX_OPEN = create("PX_OPEN", Double.class);
    public static final YahooFieldExt PX_HIGH = create("PX_HIGH", Double.class);
    public static final YahooFieldExt PX_LOW = create("PX_LOW", Double.class);
    public static final YahooFieldExt PX_CLOSE = create("PX_CLOSE", Double.class);
    public static final YahooFieldExt PX_VOLUME = create("PX_VOLUME", Double.class);
    public static final YahooFieldExt PX_CHANGE = create("PX_CHANGE", Double.class);
    public static final YahooFieldExt PX_CHANGE_PERCENT = create("PX_CHANGE_PERCENT", Double.class);
    public static final YahooFieldExt PX_52W_LOW = create("PX_52W_LOW", Double.class);
    public static final YahooFieldExt PX_52W_HIGH = create("PX_52W_HIGH", Double.class);
    public static final YahooFieldExt PX_SPLIT_RATIO = create("PX_SPLIT_RATIO", Double.class);
    public static final YahooFieldExt PX_BID = create("PX_BID", Double.class);
    public static final YahooFieldExt PX_BID_SIZE = create("PX_BID_SIZE", Double.class);
    public static final YahooFieldExt PX_ASK = create("PX_ASK", Double.class);
    public static final YahooFieldExt PX_ASK_SIZE = create("PX_ASK_SIZE", Double.class);
    public static final YahooFieldExt PX_LAST_DATE = create("PX_LAST_DATE", LocalDate.class);
    public static final YahooFieldExt PX_LAST_TIME = create("PX_LAST_TIME",LocalTime.class);
    public static final YahooFieldExt PX_LAST = create("PX_LAST", Double.class);
    public static final YahooFieldExt PX_LAST_SIZE = create("PX_LAST_SIZE", Double.class);
    public static final YahooFieldExt PX_STRIKE = create("PX_STRIKE", Double.class);
    public static final YahooFieldExt PX_PREVIOUS_CLOSE = create("PX_PREVIOUS_CLOSE", Double.class);
    public static final YahooFieldExt PX_LAST_AFTER_HOURS = create("PX_LAST_AFTER_HOURS", Double.class);
    public static final YahooFieldExt PX_CHANGE_AFTER_HOURS = create("PX_CHANGE_AFTER_HOURS", Double.class);
    public static final YahooFieldExt PX_CHANGE_52W_LOW = create("PX_CHANGE_52W_LOW", Double.class);
    public static final YahooFieldExt PX_CHANGE_52W_HIGH = create("PX_CHANGE_52W_HIGH", Double.class);
    public static final YahooFieldExt PX_CHANGE_PERCENT_52W_LOW = create("PX_CHANGE_PERCENT_52W_LOW", Double.class);
    public static final YahooFieldExt PX_CHANGE_PERCENT_52W_HIGH = create("PX_CHANGE_PERCENT_52W_HIGH", Double.class);
    public static final YahooFieldExt PX_DAYS_RANGE = create("PX_DAYS_RANGE", Double.class);
    public static final YahooFieldExt PX_MOVING_AVG_52W = create("PX_MOVING_AVG_52W", Double.class);
    public static final YahooFieldExt PX_MOVING_AVG_200D = create("PX_MOVING_AVG_200D", Double.class);
    public static final YahooFieldExt OPTION_TYPE = create("OPTION_TYPE", String.class);
    public static final YahooFieldExt EXPIRY_DATE = create("EXPIRY_DATE", LocalDate.class);
    public static final YahooFieldExt TRADE_DATE = create("TRADE_DATE", LocalDate.class);
    public static final YahooFieldExt EXCHANGE = create("EXCHANGE", String.class);
    public static final YahooFieldExt AVG_DAILY_VOLUME = create("AVG_DAILY_VOLUME", Double.class);
    public static final YahooFieldExt BOOK_VALUE = create("BOOK_VALUE", Double.class);
    public static final YahooFieldExt DIVIDEND_PER_SHARE = create("DIVIDEND_PER_SHARE", Double.class);
    public static final YahooFieldExt IMPLIED_VOLATILITY = create("IMPLIED_VOLATILITY", Double.class);
    public static final YahooFieldExt OPEN_INTEREST = create("OPEN_INTEREST", Double.class);
    public static final YahooFieldExt EPS = create("EPS", Double.class);
    public static final YahooFieldExt EPS_ESTIMATE = create("EPS_ESTIMATE", Double.class);
    public static final YahooFieldExt EPS_NEXT_YEAR = create("EPS_NEXT_YEAR", Double.class);
    public static final YahooFieldExt EPS_NEXT_QUARTER = create("EPS_NEXT_QUARTER", Double.class);
    public static final YahooFieldExt FLOAT_SHARES = create("FLOAT_SHARES", Double.class);
    public static final YahooFieldExt FIFTY_TWO_WEEK_LOW = create("FIFTY_TWO_WEEK_LOW", Double.class);
    public static final YahooFieldExt FIFTY_TWO_WEEK_HIGH = create("FIFTY_TWO_WEEK_HIGH", Double.class);
    public static final YahooFieldExt ANNUALISED_GAIN = create("ANNUALISED_GAIN", Double.class);
    public static final YahooFieldExt PE_TRAILING = create("PE_TRAILING", Double.class);
    public static final YahooFieldExt PE_FORWARD = create("PE_FORWARD", Double.class);
    public static final YahooFieldExt MARKET_CAP = create("MARKET_CAP", Double.class);
    public static final YahooFieldExt ENTERPRISE_VALUE = create("ENTERPRISE_VALUE", Double.class);
    public static final YahooFieldExt EBITDA = create("EBITDA", Double.class);
    public static final YahooFieldExt PRICE_SALES_RATIO = create("PRICE_SALES_RATIO", Double.class);
    public static final YahooFieldExt PRICE_BOOK_RATIO = create("PRICE_BOOK_RATIO", Double.class);
    public static final YahooFieldExt PEG_RATIO = create("PEG_RATIO", Double.class);
    public static final YahooFieldExt PRICE_EPS_RATIO_CURRENT_YEAR = create("PRICE_EPS_RATIO_CURRENT_YEAR", Double.class);
    public static final YahooFieldExt PRICE_EPS_RATIO_NEXT_YEAR = create("PRICE_EPS_RATIO_NEXT_YEAR", Double.class);
    public static final YahooFieldExt ENTERPRISE_VALUE_REVENUE = create("ENTERPRISE_VALUE_REVENUE", Double.class);
    public static final YahooFieldExt ENTERPRISE_VALUE_EBITDA = create("ENTERPRISE_VALUE_EBITDA", Double.class);
    public static final YahooFieldExt FISCAL_YEAR_END = create("FISCAL_YEAR_END", LocalDate.class);
    public static final YahooFieldExt MOST_RECENT_QUARTER = create("MOST_RECENT_QUARTER", LocalDate.class);
    public static final YahooFieldExt PROFIT_MARGIN = create("PROFIT_MARGIN", Double.class);
    public static final YahooFieldExt OPERATING_MARGIN = create("OPERATING_MARGIN", Double.class);
    public static final YahooFieldExt RETURN_ON_ASSETS = create("RETURN_ON_ASSETS", Double.class);
    public static final YahooFieldExt RETURN_ON_EQUITY = create("RETURN_ON_EQUITY", Double.class);
    public static final YahooFieldExt REVENUE_TTM = create("REVENUE_TTM", Double.class);
    public static final YahooFieldExt REVENUE_PER_SHARE = create("REVENUE_PER_SHARE", Double.class);
    public static final YahooFieldExt REVENUE_GROWTH_QTLY = create("REVENUE_GROWTH_QTLY", Double.class);
    public static final YahooFieldExt GROSS_PROFIT = create("GROSS_PROFIT", Double.class);
    public static final YahooFieldExt EBITDA_TTM = create("EBITDA_TTM", Double.class);
    public static final YahooFieldExt EPS_DILUTED = create("EPS_DILUTED", Double.class);
    public static final YahooFieldExt EARNINGS_GRWOTH_QTLY = create("EARNINGS_GRWOTH_QTLY", Double.class);
    public static final YahooFieldExt CASH_MRQ = create("CASH_MRQ", Double.class);
    public static final YahooFieldExt CASH_PER_SHARE = create("CASH_PER_SHARE", Double.class);
    public static final YahooFieldExt DEBT_MRQ = create("DEBT_MRQ", Double.class);
    public static final YahooFieldExt DEBT_OVER_EQUITY_MRQ = create("DEBT_OVER_EQUITY_MRQ", Double.class);
    public static final YahooFieldExt CURRENT_RATIO = create("CURRENT_RATIO", Double.class);
    public static final YahooFieldExt BOOK_VALUE_PER_SHARE = create("BOOK_VALUE_PER_SHARE", Double.class);
    public static final YahooFieldExt OPERATING_CASH_FLOW = create("OPERATING_CASH_FLOW", Double.class);
    public static final YahooFieldExt LEVERED_FREE_CASH_FLOW = create("LEVERED_FREE_CASH_FLOW", Double.class);
    public static final YahooFieldExt ADV_3MONTH = create("ADV_3MONTH", Double.class);
    public static final YahooFieldExt ADV_10DAY = create("ADV_10DAY", Double.class);
    public static final YahooFieldExt SHARES_OUTSTANDING = create("SHARES_OUTSTANDING", Double.class);
    public static final YahooFieldExt SHARES_FLOAT = create("SHARES_FLOAT", Double.class);
    public static final YahooFieldExt OWNER_PERCENT_INSIDER = create("OWNER_PERCENT_INSIDER", Double.class);
    public static final YahooFieldExt OWNER_PERCENT_INSTITUTION = create("OWNER_PERCENT_INSTITUTION", Double.class);
    public static final YahooFieldExt SHARES_SHORT = create("SHARES_SHORT", Double.class);
    public static final YahooFieldExt SHARES_SHORT_RATIO = create("SHARES_SHORT_RATIO", Double.class);
    public static final YahooFieldExt SHARES_SHORT_PRIOR = create("SHARES_SHORT_PRIOR", Double.class);
    public static final YahooFieldExt BETA = create("BETA", Double.class);
    public static final YahooFieldExt DIVIDEND_PAY_DATE = create("DIVIDEND_PAY_DATE", LocalDate.class);
    public static final YahooFieldExt DIVIDEND_FWD = create("DIVIDEND_FWD", Double.class);
    public static final YahooFieldExt DIVIDEND_FWD_YIELD = create("DIVIDEND_FWD_YIELD", Double.class);
    public static final YahooFieldExt DIVIDEND_TRAILING = create("DIVIDEND_TRAILING", Double.class);
    public static final YahooFieldExt DIVIDEND_TRAILING_YIELD = create("DIVIDEND_TRAILING_YIELD", Double.class);
    public static final YahooFieldExt DIVIDEND_PAYOUT_RATIO = create("DIVIDEND_PAYOUT_RATIO", Double.class);
    public static final YahooFieldExt DIVIDEND_EX_DATE = create("DIVIDEND_EX_DATE", LocalDate.class);
    public static final YahooFieldExt LAST_SPLIT_DATE = create("LAST_SPLIT_DATE", LocalDate.class);
    public static final YahooFieldExt EX_DIVIDEND_DATE = create("EX_DIVIDEND_DATE", LocalDate.class);
    public static final YahooFieldExt PRICE_EARNINGS_RATIO = create("PRICE_EARNINGS_RATIO", Double.class);
    public static final YahooFieldExt SHORT_RATIO = create("SHORT_RATIO", Double.class);

    public static final YahooFieldExt EARNINGS_TOTAL_REVENUE = create("TOTAL_REVENUE", Double.class);
    public static final YahooFieldExt COST_OF_REVENUE = create("COST_OF_REVENUE", Double.class);
    public static final YahooFieldExt EARNINGS_GROSS_PROFIT = create("EARNINGS_GROSS_PROFIT", Double.class);
    public static final YahooFieldExt OPERATING_REVENUE = create("OPERATING_REVENUE", Double.class);
    public static final YahooFieldExt OPERATING_INCOME = create("OPERATING_INCOME", Double.class);
    public static final YahooFieldExt NET_INCOME = create("NET_INCOME", Double.class);
    public static final YahooFieldExt RESEARCH_AND_DEVELOPMENT = create("RESEARCH_AND_DEVELOPMENT", Double.class);
    public static final YahooFieldExt OPERATING_EXPENSE = create("OPERATING_EXPENSE", Double.class);
    public static final YahooFieldExt CURRENT_ASSETS = create("CURRENT_ASSETS", Double.class);
    public static final YahooFieldExt TOTAL_ASSETS = create("TOTAL_ASSETS", Double.class);
    public static final YahooFieldExt TOTAL_LIABILITIES = create("TOTAL_LIABILITIES", Double.class);
    public static final YahooFieldExt CURRENT_CASH = create("CURRENT_CASH", Double.class);
    public static final YahooFieldExt CURRENT_DEBT = create("CURRENT_DEBT", Double.class);
    public static final YahooFieldExt TOTAL_CASH = create("TOTAL_CASH", Double.class);
    public static final YahooFieldExt TOTAL_DEBT = create("TOTAL_DEBT", Double.class);
    public static final YahooFieldExt SHARE_HOLDER_EQUITY = create("SHARE_HOLDER_EQUITY", Double.class);
    public static final YahooFieldExt CASH_CHANGE = create("CASH_CHANGE", Double.class);
    public static final YahooFieldExt CASH_FLOW = create("CASH_FLOW", Double.class);
    public static final YahooFieldExt OPERATING_GAINS_LOSSES = create("OPEATING_GAINS_LOSSES", Double.class);
    public static final YahooFieldExt REPORT_DATE = create("REPORT_DATE", LocalDate.class);
    public static final YahooFieldExt ACTUAL_EPS = create("ACTUAL_EPS", Double.class);
    public static final YahooFieldExt CONSENSUS_EPS = create("CONSENSUS_EPS", Double.class);
    public static final YahooFieldExt ESTIMATED_EPS = create("ESTIMATED_EPS", Double.class);
    public static final YahooFieldExt ANNOUNCE_TIME = create("ANNOUNCE_TIME", Double.class);
    public static final YahooFieldExt NUMBER_OF_ESTIMATES = create("NUMBER_OF_ESTIMATES", Double.class);
    public static final YahooFieldExt EPS_SURPRISE_DOLLAR = create("EPS_SURPRISE_DOLLAR", Double.class);
    public static final YahooFieldExt EPS_REPORT_DATE = create("EPS_REPORT_DATE", LocalDate.class);
    public static final YahooFieldExt FISCAL_END_DATE = create("FISCAL_END_DATE", LocalDate.class);
    public static final YahooFieldExt FISCAL_PERIOD = create("FISCAL_PERIOD", Double.class);
    public static final YahooFieldExt YEAR_AGO = create("YEAR_AGO", Double.class);
    public static final YahooFieldExt PREFERRED_SHARES = create("PREFERRED_SHARES", Double.class);
    public static final YahooFieldExt COMMON_SHARES = create("COMMON_SHARES", Double.class);
    public static final YahooFieldExt TREASURY_SHARES = create("TREASURY_SHARES", Double.class);
    public static final YahooFieldExt OUTSTANDING_SHARES = create("OUTSTANDING_SHARES", Double.class);
    public static final YahooFieldExt COMPANY_NAME = create("COMPANY_NAME", String.class);
    public static final YahooFieldExt COMPANY_DESC = create("COMPANY_DESC", String.class);
    public static final YahooFieldExt COMPANY_CITY = create("COMPANY_CITY", String.class);
    public static final YahooFieldExt COMPANY_STATE = create("COMPANY_STATE", String.class);
    public static final YahooFieldExt COMPANY_COUNTRY = create("COMPANY_COUNTRY", String.class);
    public static final YahooFieldExt COMPANY_SECTOR = create("COMPANY_SECTOR", String.class);
    public static final YahooFieldExt COMPANY_INDUSTRY = create("COMPANY_INDUSTY", String.class);

}