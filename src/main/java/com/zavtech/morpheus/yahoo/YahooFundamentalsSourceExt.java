/**
 * Copyright (C) 2014-2017 Xavier Witdouck
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zavtech.morpheus.yahoo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import com.zavtech.morpheus.array.Array;
import com.zavtech.morpheus.frame.DataFrame;
import com.zavtech.morpheus.frame.DataFrameException;
import com.zavtech.morpheus.frame.DataFrameSource;
import com.zavtech.morpheus.util.Collect;
import com.zavtech.morpheus.util.http.HttpClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Any use of the extracted data from this software should adhere to Yahoo
 * Finance Terms and Conditions.
 *
 * @author Xavier Witdouck
 *
 *         <p>
 *         <strong>This is open source software released under the
 *         <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache 2.0
 *         License</a></strong>
 *         </p>
 */
@Component
public class YahooFundamentalsSourceExt extends DataFrameSource<String, YahooFieldExt, YahooFundamentalsSourceExt.Options> {

    private static final Array<YahooFieldExt> fields = Array.of(
        YahooFieldExt.COMPANY_NAME,
        YahooFieldExt.COMPANY_DESC,
        YahooFieldExt.COMPANY_CITY,
        YahooFieldExt.COMPANY_STATE,
        YahooFieldExt.COMPANY_COUNTRY,
        YahooFieldExt.COMPANY_SECTOR,
        YahooFieldExt.COMPANY_INDUSTRY
    );

    private String baseUrl;
    private String urlTemplate;

    /**
     * Constructor
     */
    public YahooFundamentalsSourceExt() {
        this("https://finance.yahoo.com/quote/%s/profile?p=%s");
    }

    /**
     * Constructor
     * @param urlTemplate   the URL template on which to replace the security ticker
     */
    public YahooFundamentalsSourceExt(String urlTemplate) {
        this.urlTemplate = urlTemplate;
        this.baseUrl = urlTemplate.substring(0, urlTemplate.indexOf('/', 9));
    }

    /**
     * Returns the fields supported by this source
     * @return  the fields supported by this source
     */
    Array<YahooFieldExt> getFields() {
        return fields;
    }

    @Override
    public DataFrame<String, YahooFieldExt> read(Consumer<Options> configurator) throws DataFrameException {
        final Options options = initOptions(new Options(), configurator);
        final Set<String> tickers = options.tickers;
        try {
            final DataFrame<String, YahooFieldExt> frame = createFrame(tickers);
            tickers.parallelStream().forEach(ticker -> {
                this.captureIncome(ticker, frame);
            });
            
            return frame;
        } catch (Exception ex) {
            throw new DataFrameException("Failed to query statistics for one or more tickers: " + tickers, ex);
        }
    }

    /**
     * Returns the DataFrame to capture results for request
     * @param tickers   the tickers
     * @return          the DataFrame result
     */
    private DataFrame<String,YahooFieldExt> createFrame(Set<String> tickers) {
        return DataFrame.of(tickers, YahooFieldExt.class, columns -> {
            fields.forEach(field -> {
                final Class<?> dataType = field.getDataType();
                final Array<?> array = Array.of(dataType, tickers.size());
                columns.add(field, array);
            });
        });
    }

    /**
     * Captures Income Info from the document
     * @param endingDate    the asset ticker
     * @param document  the document
     * @param frame    the result matrix
     */
    private void captureIncome(String ticker, DataFrame<String, YahooFieldExt> frame) {
        try {
            final String url = String.format(urlTemplate, ticker, ticker);
            HttpClient.getDefault().doGet(httpRequest -> {
                httpRequest.setUrl(url);
                httpRequest.getHeaders().putAll(YahooFinanceExt.getRequestHeaders());
                httpRequest.setResponseHandler(response -> {
                    try {
                        final long t1 = System.currentTimeMillis();
                        final InputStream stream = response.getStream();
                        final Document document = Jsoup.parse(stream, "UTF-8", baseUrl);
                        final long t2 = System.currentTimeMillis();
                        
                        this.parseDesc(document, frame, ticker);
                        this.parseSector(document, frame, ticker);
                        this.parseAddress(document, frame, ticker);

                        final long t3 = System.currentTimeMillis();
                        System.out.println("Processed " + url + " in " + (t3 - t1) + " millis (" + (t2 - t1) + "+" + (t3 - t2) + " millis)");
                        return Optional.empty();
                    } catch (IOException ex) {
                        throw new RuntimeException("Failed to load content from Yahoo Finance: " + url, ex);
                    }
                });
            });
            
        } catch (Exception ex) {
            throw new DataFrameException("Failed to extract statistic for " + ticker, ex);
        }
    }

    private void parseDesc(Document document, DataFrame<String, YahooFieldExt> frame, String ticker) {
        final Elements elements = document.select("section.quote-sub-section").select("p");
        frame.data().setValue(ticker, YahooFieldExt.COMPANY_DESC, elements.get(0).text());
        
    }

    private void parseSector(Document document, DataFrame<String, YahooFieldExt> frame, String ticker) {
        Elements elements = document.select("div.asset-profile-container").select("div");
        String name = elements.select("h3").get(0).text();
        elements = elements.select("div");
        elements = elements.select("p").get(1).select("span");
        frame.data().setValue(ticker, YahooFieldExt.COMPANY_NAME, name);
        frame.data().setValue(ticker, YahooFieldExt.COMPANY_SECTOR, elements.get(1).text());
        frame.data().setValue(ticker, YahooFieldExt.COMPANY_INDUSTRY, elements.get(3).text());
    } 

    private void parseAddress(Document document, DataFrame<String, YahooFieldExt> frame, String ticker) {
        Elements elements = document.select("div.asset-profile-container").select("div");
        //String name = elements.select("h3").get(0).text();
        elements = elements.select("div");
        String address = elements.select("p").get(0).text();
        frame.data().setValue(ticker, YahooFieldExt.COMPANY_CITY, address);
    }

    
    /**
     * The options for this source
     */
    public class Options implements DataFrameSource.Options<String,YahooFieldExt> {

        private Set<String> tickers = new HashSet<>();


        @Override
        public void validate() {
            if (tickers == null) throw new IllegalStateException("The set of tickers cannot be null");
            if (tickers.size() == 0) throw new IllegalStateException("At least one ticker must be specified");
        }

        /**
         * Sets the tickers for these options
         * @param tickers   the tickers
         */
        public Options withTickers(String...tickers) {
            this.tickers.addAll(Arrays.asList(tickers));
            return this;
        }

        /**
         * Sets the tickers for these options
         * @param tickers   the tickers
         */
        public Options withTickers(Iterable<String> tickers) {
            this.tickers.addAll(Collect.asList(tickers));
            return this;
        }
    }

    public static void main(String[] args) {
        final String ticker = "AAPL";
        final YahooFundamentalsSourceExt source = new YahooFundamentalsSourceExt();
        final DataFrame<String, YahooFieldExt> frame = source.read(o -> o.withTickers(ticker));
        frame.out().print();
    }

    static final Logger logger = LoggerFactory.getLogger(YahooFundamentalsSourceExt.class);
}
