
package com.eod_historical_data;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EodIncomeStatement {

    public EodIncomeStatement(EodIncomeRecordList quarterly, EodIncomeRecordList yearly) {
		this.quarterly = quarterly;
		this.yearly = yearly;
	}

	public EodIncomeStatement() {
		
	}
    
    @SerializedName("quarterly")
    @Expose
    private EodIncomeRecordList quarterly;
    @SerializedName("yearly")
    @Expose
    private EodIncomeRecordList yearly;

    public EodIncomeRecordList getQuarterly() {
        return quarterly;
    }

    public void setQuarterly(EodIncomeRecordList quarterly) {
        this.quarterly = quarterly;
    }

    public EodIncomeRecordList getYearly() {
        return yearly;
    }

    public void setYearly(EodIncomeRecordList yearly) {
        this.yearly = yearly;
    }

    


 
    }

