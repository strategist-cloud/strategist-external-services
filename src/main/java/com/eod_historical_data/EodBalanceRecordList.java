package com.eod_historical_data;

import com.google.gson.annotations.Expose;

public class EodBalanceRecordList {

	@Expose public  EodBalanceRecord balanceRecord[];

    public EodBalanceRecordList() {
		
	}
    
    public EodBalanceRecordList(EodBalanceRecord[] balanceRecord){
        this.balanceRecord = balanceRecord;

    }
}