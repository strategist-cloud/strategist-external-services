package com.eod_historical_data;

import com.google.gson.annotations.Expose;

public class EodEarningsRecordList {


	@Expose public  EodEarningsRecord earningsRecord[];

	public EodEarningsRecordList() {
		
	}

	public EodEarningsRecordList(EodEarningsRecord[] earningsRecord){
		this.earningsRecord = earningsRecord;

	}
}