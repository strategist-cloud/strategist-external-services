
package com.eod_historical_data;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EodBalanceSheet {

    public EodBalanceSheet(EodBalanceRecordList quarterly, EodBalanceRecordList yearly) {
		this.quarterly = quarterly;
		this.yearly = yearly;
	}

	public EodBalanceSheet() {
		
	}
 

    @SerializedName("quarterly")
    @Expose private EodBalanceRecordList quarterly;

    @SerializedName("yearly")
    @Expose private EodBalanceRecordList yearly;

    public EodBalanceRecordList getQuarterly() {
        return quarterly;
    }

    public void setQuarterly(EodBalanceRecordList quarterly) {
        this.quarterly = quarterly;
    }

    public EodBalanceRecordList getYearly() {
        return yearly;
    }

    public void setYearly(EodBalanceRecordList yearly) {
        this.yearly = yearly;
    }



 
}
