
package com.eod_historical_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EodFinancials {

    @SerializedName("Balance_Sheet")
    @Expose
    private EodBalanceSheet balanceSheet;
    @SerializedName("Cash_Flow")
    @Expose
    private EodCashFlow cashFlow;
    @SerializedName("Income_Statement")
    @Expose
    private EodIncomeStatement incomeStatement;

    public EodBalanceSheet getBalanceSheet() {
        return balanceSheet;
    }

    public void setBalanceSheet(EodBalanceSheet balanceSheet) {
        this.balanceSheet = balanceSheet;
    }

    public EodCashFlow getCashFlow() {
        return cashFlow;
    }

    public void setCashFlow(EodCashFlow cashFlow) {
        this.cashFlow = cashFlow;
    }

    public EodIncomeStatement getIncomeStatement() {
        return incomeStatement;
    }

    public void setIncomeStatement(EodIncomeStatement incomeStatement) {
        this.incomeStatement = incomeStatement;
    }

}
