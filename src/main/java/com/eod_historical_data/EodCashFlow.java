
package com.eod_historical_data;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EodCashFlow {

    public EodCashFlow(EodCashFlowRecordList quarterly, EodCashFlowRecordList yearly) {
		this.quarterly = quarterly;
		this.yearly = yearly;
	}

	public EodCashFlow() {
		
	}
 
    @SerializedName("quarterly")
    
    // @Expose private CashFlowRecordList quarterly;
	@Expose public  EodCashFlowRecordList quarterly;
	
    @SerializedName("yearly")
    //@Expose private CashFlowRecordList yearly;
	@Expose public  EodCashFlowRecordList yearly;

    public EodCashFlowRecordList getQuarterly() {
        return quarterly;
    }

    public void setQuarterly(EodCashFlowRecordList quarterly) {
        this.quarterly = quarterly;
    }

    public EodCashFlowRecordList getYearly() {
        return yearly;
    }

    public void setYearly(EodCashFlowRecordList yearly) {
        this.yearly = yearly;
    }


 
}
