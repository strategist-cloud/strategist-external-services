
package com.eod_historical_data;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EodEarnings {
	
	@SerializedName("History")
	@Expose
	private EodEarningsRecordList history = null;
	
	@SerializedName("Trend")
	@Expose
	private EodEarningsRecordList trend = null;

	public EodEarningsRecordList getHistory() {
	return history;
	}

	public void setHistory(EodEarningsRecordList history) {
	this.history = history;
	}

	public EodEarningsRecordList getTrend() {
	return trend;
	}

	public void setTrend(EodEarningsRecordList trend) {
	this.trend = trend;
	}
	/*
    @SerializedName("History")
    @Expose
    private History history;
    
	@Expose public  EarningsRecord earningsRecord[];

    @SerializedName("Trend")
    @Expose
    private Trend trend;

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public Trend getTrend() {
        return trend;
    }

    public void setTrend(Trend trend) {
        this.trend = trend;
    }
*/
}
