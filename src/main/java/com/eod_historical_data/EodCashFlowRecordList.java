package com.eod_historical_data;

import com.google.gson.annotations.Expose;

public class EodCashFlowRecordList {

    	@Expose public  EodCashFlowRecord cashFlowRecord[];

        public EodCashFlowRecordList() {
    		
    	}
        
        public EodCashFlowRecordList(EodCashFlowRecord[] cashFlowRecord){
            this.cashFlowRecord = cashFlowRecord;

        }
    }