
package com.eod_historical_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EodStock {

    @SerializedName("General")
    @Expose
    private EodGeneral general;
    @SerializedName("Highlights")
    @Expose
    private EodHighlights highlights;
    @SerializedName("Earnings")
    @Expose
    private EodEarnings earnings;
    @SerializedName("Financials")
    @Expose
    private EodFinancials financials;

    public EodGeneral getGeneral() {
        return general;
    }

    public void setGeneral(EodGeneral general) {
        this.general = general;
    }

    public EodHighlights getHighlights() {
        return highlights;
    }

    public void setHighlights(EodHighlights highlights) {
        this.highlights = highlights;
    }

    public EodEarnings getEarnings() {
        return earnings;
    }

    public void setEarnings(EodEarnings earnings) {
        this.earnings = earnings;
    }

    public EodFinancials getFinancials() {
        return financials;
    }

    public void setFinancials(EodFinancials financials) {
        this.financials = financials;
    }

}
