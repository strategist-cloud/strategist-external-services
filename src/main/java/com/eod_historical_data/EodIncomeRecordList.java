package com.eod_historical_data;

import com.google.gson.annotations.Expose;

public class EodIncomeRecordList {


	@Expose public  EodIncomeRecord incomeRecord[];

	public EodIncomeRecordList() {
		
	}

	public EodIncomeRecordList(EodIncomeRecord[] incomeRecord){
		this.incomeRecord = incomeRecord;

	}
}